<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Observer;

use GuzzleHttp\Client;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Logger\Logger;

class OrderPlaced implements ObserverInterface
{
    private const ATHENA_USER_COOKIE = '_athena';

    private ConfigHelper $configHelper;

    private PhpCookieManager $cookieManager;

    private SerializerInterface $serializer;

    private Logger $logger;

    public function __construct(
        ConfigHelper $configHelper,
        PhpCookieManager $cookieManager,
        SerializerInterface $serializer,
        Logger $logger
    ) {
        $this->configHelper = $configHelper;
        $this->cookieManager = $cookieManager;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    /**
     * Dispatch placed order data
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        if (!$this->configHelper->getAthenaSearchStatus()) {
            return;
        }

        /** @var Order $order */
        $order = $observer->getOrder();
        $websiteToken = $this->configHelper->getWebsiteToken();

        $ordersData['token'] = $websiteToken;
        $ordersData['userToken'] = $this->cookieManager->getCookie(self::ATHENA_USER_COOKIE) ?? '';
        $ordersData['order'] = [
            'quote_id' => (int)$order->getQuoteId(),
            'increment_id' => $order->getIncrementId(),
            'store_id' => $order->getStoreId(),
            'status' => $order->getStatus(),
            'order_currency_code' => $order->getOrderCurrencyCode(),
            'base_grand_total' => $order->getBaseGrandTotal(),
            'subtotal_price' => $order->getSubtotal(),
            'tax_price' => $order->getTaxAmount(),
            'discount_price' => $order->getDiscountAmount(),
            'shipping_price' => $order->getShippingAmount(),
            'items' => $this->getItems($order->getAllVisibleItems())
        ];

        try {
            $client = new Client();
            $client->post(
                $this->configHelper->getEndpointUrl('order', ConfigHelper::API_V2),
                [
                    'headers' => $this->configHelper->createRequestHeader(ConfigHelper::API_V2),
                    'body' => $this->serializer->serialize($ordersData)
                ]
            );
        } catch (\Exception $exception) {
            $this->logger->logMessage($exception);
        }
    }

    /**
     * Get order items data
     *
     * @param array $orderItems
     * @return array
     */
    private function getItems(array $orderItems): array
    {
        $itemsData = [];
        /** @var OrderItemInterface $orderItem */
        foreach ($orderItems as $orderItem) {
            if (!$orderItem->getData('parent_item')) {
                $itemsData[] = [
                    'name' => $orderItem->getName(),
                    'id' => $orderItem->getProductId(),
                    'sku' => $orderItem->getSku(),
                    'type' => $orderItem->getProductType(),
                    'qty' => $orderItem->getQtyOrdered(),
                    'price_incl_tax' => $orderItem->getPriceInclTax(),
                    'price_with_qty_incl_tax' => $orderItem->getRowTotalInclTax(),
                    'price' => $orderItem->getOriginalPrice(),
                    'row_total' => $orderItem->getBaseRowTotal()
                ];
            }
        }
        return $itemsData;
    }
}
