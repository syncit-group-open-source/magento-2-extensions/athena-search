<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Controller\V2;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use GuzzleHttp\ClientFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Logger\Logger;
use Magento\Framework\Controller\ResultFactory;

class Athena implements HttpGetActionInterface, HttpPostActionInterface, CsrfAwareActionInterface
{
    public const CALL_ENDPOINT = 'endpoint';

    private ConfigHelper $configHelper;

    private RequestInterface $request;

    private ClientFactory $clientFactory;

    private SerializerInterface $serializer;

    private ResultFactory $resultFactory;

    private Logger $logger;

    public function __construct(
        ConfigHelper $configHelper,
        RequestInterface $request,
        ClientFactory $clientFactory,
        SerializerInterface $serializer,
        ResultFactory $resultFactory,
        Logger $logger
    ) {
        $this->configHelper = $configHelper;
        $this->request = $request;
        $this->clientFactory = $clientFactory;
        $this->serializer = $serializer;
        $this->resultFactory = $resultFactory;
        $this->logger = $logger;
    }

    /**
     * @return ResponseInterface|Json|ResultInterface|void
     */
    public function execute(): ?Json
    {
        $stream = null;
        $endpointUrl = $this->configHelper->getEndpointUrl(
            (string) $this->request->getParam(self::CALL_ENDPOINT),
            ConfigHelper::API_V2
        );

        [$method, $params] = $this->getBodyParams();

        if ($endpointUrl) {

            $client = $this->clientFactory->create();

            try {
                $response = $client->$method(
                    $endpointUrl,
                    [
                        'headers' => $this->configHelper->createRequestHeader(ConfigHelper::API_V2),
                        'body' => $params
                    ]
                );
                $stream = $response->getBody();
            } catch (\Exception $exception) {
                $this->logger->logMessage($exception);
            }
        }

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        return isset($stream)
            ? $resultPage->setJsonData(preg_replace('/[[:cntrl:]]/', '', $stream->getContents()))
            : $resultPage->setData([]);
    }

    /**
     * Get request body parameters
     *
     * @return void
     */
    private function getBodyParams(): array
    {
        $params = $this->request->getParams();
        unset($params[self::CALL_ENDPOINT]);
        $params = $this->serializer->serialize($params);

        return [
            $this->request->isPost() ? 'post' : 'get',
            $params
        ];
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
