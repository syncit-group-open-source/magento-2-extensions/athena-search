<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://www.syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs,
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright 2021 (C) SyncIt (https://www.syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://www.syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Syncitgroup\AthenaSearch\Helper\Config;
use Magento\Framework\UrlInterface;

/**
 * Class Index | main route for search landing page
 */
class Index implements HttpGetActionInterface
{
    private const QUERY_PARAM = 'q';

    private const ATHENA_HANDLE = 'athena_index_index_';

    private PageFactory $pageFactory;

    private RequestInterface $request;

    private Config $configHelper;

    private UrlInterface $url;

    public function __construct(
        PageFactory $pageFactory,
        RequestInterface $request,
        Config $configHelper,
        UrlInterface $url
    ) {
        $this->pageFactory = $pageFactory;
        $this->request = $request;
        $this->configHelper = $configHelper;
        $this->url = $url;
    }

    /**
     * @return Page|ResultInterface
     */
    public function execute()
    {
        $keywords = $this->request->getParam(self::QUERY_PARAM);
        $placeholder = __('Search results for %1', $keywords);

        $resultPage = $this->pageFactory->create();
        $resultPage->addHandle(self::ATHENA_HANDLE . $this->configHelper->getLayoutHandleSuffix());
        $resultPage->getConfig()->getTitle()->set($placeholder);

        $breadcrumbs = $resultPage->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb(
            'home',
            [
                'label' => __('Home'),
                'title' => __('Home'),
                'link' => $this->url->getUrl()
            ]
        );
        $breadcrumbs->addCrumb(
            'athena_search',
            [
                'label' => __($placeholder),
                'title' => __($placeholder)
            ]
        );

        return $resultPage;
    }
}
