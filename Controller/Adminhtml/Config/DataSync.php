<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Controller\Adminhtml\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\Controller\ResultFactory;

class DataSync extends Action implements HttpPostActionInterface
{
    private WriterInterface $configWriter;

    private CacheManager $cacheManager;

    public function __construct(
        Context $context,
        WriterInterface $configWriter,
        CacheManager $cacheManager
    ) {
        $this->configWriter = $configWriter;
        $this->cacheManager = $cacheManager;
        parent::__construct($context);
    }

    /**
     * @inerhitDoc
     */
    public function execute()
    {
        try {
            $status = $this->getRequest()->getParam('data_sync_status');

            $this->configWriter->save(
                'athena_search/general/data_sync_enabled',
                $this->getRequest()->getParam('data_sync_status')
            );
            $this->cacheManager->clean(['config']);
            $this->messageManager->addNoticeMessage(
                __('Athena data sync %1', $status ? __('enabled') : __('disabled'))
            );
        } catch (\Exception $exception) {
            $this->messageManager->addWarningMessage(__($exception->getMessage()));
        }

        return $this->resultFactory->create(ResultFactory::TYPE_RAW);
    }
}
