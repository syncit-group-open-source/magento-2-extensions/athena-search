<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Jovana Brankovic <jovana.brankovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Plugin;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product as BaseProductResource;
use Syncitgroup\AthenaSearch\Job\Publisher as AthenaJobPublisher;
use Syncitgroup\AthenaSearch\Model\Indexer\ProductSync;

class ProductDisabledResource
{
    private AthenaJobPublisher $athenaJobPublisher;

    public function __construct(
        AthenaJobPublisher $athenaJobPublisher
    ) {
        $this->athenaJobPublisher = $athenaJobPublisher;
    }

    /**
     * Push disabled product to sync to athena
     *
     * @param BaseProductResource $productResource
     * @param $result
     * @param Product $product
     * @return mixed
     */
    public function afterSave(BaseProductResource $productResource, $result, Product $product)
    {
        $originalStatus = (int)$product->getOrigData('status');
        if((int) $product->getVisibility() === Visibility::VISIBILITY_BOTH
            && $originalStatus === Status::STATUS_ENABLED
            && (int) $product->getStatus() === Status::STATUS_DISABLED) {
            $this->athenaJobPublisher->publishJob(
                ProductSync::CODE,
                'pushDeletedEntity',
                [$product->getId()]
            );
        }
        return $result;
    }
}
