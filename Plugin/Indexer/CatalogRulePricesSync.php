<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Plugin\Indexer;

use Magento\Catalog\Model\Indexer\Product\Price as PriceIndexer;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Logger\Logger;
use Syncitgroup\AthenaSearch\Model\Indexer\Flag\CatalogRuleJobFlagFactory;
use Syncitgroup\AthenaSearch\Model\Indexer\CatalogRulePriceSync;

class CatalogRulePricesSync
{
    private ConfigHelper $configHelper;

    private CatalogRuleJobFlagFactory $catalogRuleJobFlagFactory;

    private CatalogRulePriceSync $catalogRulePriceSync;

    private Logger $logger;

    public function __construct(
        ConfigHelper $configHelper,
        CatalogRuleJobFlagFactory $catalogRuleJobFlagFactory,
        CatalogRulePriceSync $catalogRulePriceSync,
        Logger $logger
    ) {
        $this->configHelper = $configHelper;
        $this->catalogRuleJobFlagFactory = $catalogRuleJobFlagFactory;
        $this->catalogRulePriceSync = $catalogRulePriceSync;
        $this->logger = $logger;
    }

    /**
     * Publish catalogrule_price_sync if flag is set
     *
     * @param PriceIndexer $productPriceIndex
     * @return void
     */
    public function afterExecuteFull(PriceIndexer $productPriceIndex)
    {
        if ($this->configHelper->getAthenaEnabledStores()) {
            try {
                $flag = $this->catalogRuleJobFlagFactory->create()->loadSelf();
                if ($flag->getState()) {
                    $this->catalogRulePriceSync->execute(null);
                    $flag->setState(0)->save();
                }
            } catch (\Exception $exception) {
                $this->logger->logMessage($exception);
            }
        }
    }
}
