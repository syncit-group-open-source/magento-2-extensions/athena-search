<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Plugin;

use Magento\Catalog\Model\Category as BaseCategory;
use Magento\Catalog\Model\ResourceModel\Category as BaseCategoryResource;
use Magento\Framework\Indexer\IndexerRegistry;
use Syncitgroup\AthenaSearch\Model\Indexer\CategorySync;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Job\Publisher;

class CategoryResource
{
    private const WATCH_DATA_CHANGE_FOR = [
        'name',
        'path',
        'url',
        'is_active',
        'is_anchor',
        'image'
    ];

    private $athenaCategoryIndexer;

    private ConfigHelper $configHelper;

    private Publisher $athenaJobPublisher;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        ConfigHelper $configHelper,
        Publisher $athenaJobPublisher
    ) {
        $this->athenaCategoryIndexer = $indexerRegistry->get(CategorySync::ATHENA_CATEGORY_INDEX);
        $this->configHelper = $configHelper;
        $this->athenaJobPublisher = $athenaJobPublisher;
    }

    /**
     * Publish category sync if it has data changes on watched fields
     *
     * @param BaseCategoryResource $categoryResource
     * @param $result
     * @param $category
     * @return mixed
     */
    public function afterSave(BaseCategoryResource $categoryResource, $result, $category)
    {
        if (!$this->configHelper->getAthenaEnabledStores() && !$this->shouldAddCallback($category)) {
            return $result;
        }

        $categoryResource->addCommitCallback(function () use ($category) {
            if (!$this->athenaCategoryIndexer->isScheduled()) {
                $this->athenaJobPublisher->publishJob(
                    CategorySync::CODE,
                    'execute',
                    null
                );
            }
        });

        return $result;
    }

    /**
     * Add callback for deleted categories
     *
     * @param BaseCategoryResource $categoryResource
     * @param $result
     * @param $category
     * @return mixed
     */
    public function afterDelete(BaseCategoryResource $categoryResource, $result, $category)
    {
        if (!$this->configHelper->getAthenaEnabledStores()) {
            return $result;
        }

        $categoryResource->addCommitCallback(function () use ($category) {
            $this->athenaJobPublisher->publishJob(
                CategorySync::CODE,
                'execute',
                null
            );
        });

        return $result;
    }

    /**
     * Check if it has data changes on watched attributes
     *
     * @param BaseCategory $category
     * @return bool
     */
    private function shouldAddCallback(BaseCategory $category): bool
    {
        foreach (self::WATCH_DATA_CHANGE_FOR as $attributeCode) {
            if ($category->getOrigData($attributeCode) !== $category->getData($attributeCode)) {
                return true;
            }
        }

        return false;
    }
}
