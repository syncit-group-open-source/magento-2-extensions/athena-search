<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Plugin;

use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product as BaseProductResource;
use Magento\Framework\Indexer\IndexerRegistry;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Job\Publisher;
use Syncitgroup\AthenaSearch\Model\Indexer\ProductSync;

class ProductResource
{
    private $athenaProductIndexer;

    private ConfigHelper $configHelper;

    private Publisher $athenaJobPublisher;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        ConfigHelper $configHelper,
        Publisher $athenaJobPublisher
    ) {
        $this->athenaProductIndexer = $indexerRegistry->get(ProductSync::ATHENA_PRODUCT_INDEXER);
        $this->configHelper = $configHelper;
        $this->athenaJobPublisher = $athenaJobPublisher;
    }

    /**
     * After save handle
     *
     * @param BaseProductResource $productResource
     * @param $result
     * @param $product
     * @return mixed
     */
    public function afterSave(BaseProductResource $productResource, $result, $product)
    {
        if ((int) $product->getVisibility() === Visibility::VISIBILITY_BOTH
            && (int) $product->getStatus() === Status::STATUS_ENABLED) {
            $this->pushJob([$product->getId()]);
        }

        return $result;
    }

    /**
     * After delete handle
     *
     * @param BaseProductResource $productResource
     * @param $result
     * @param $product
     * @return mixed
     */
    public function afterDelete(BaseProductResource $productResource, $result, $product)
    {
        $this->pushJob([$product->getId()], 'pushDeletedEntity');
        return $result;
    }

    /**
     * After attribute update handle
     *
     * @param Action $subject
     * @param $result
     * @param $productIds
     * @return mixed
     */
    public function afterUpdateAttributes(Action $subject, $result, $productIds)
    {
        $this->pushJob($productIds);
        return $result;
    }

    /**
     * After website update
     *
     * @param Action $subject
     * @param $result
     * @param $productIds
     * @return mixed
     */
    public function afterUpdateWebsites(Action $subject, $result, $productIds)
    {
        $this->pushJob($productIds);
        return $result;
    }

    /**
     * Push job to queue execution
     *
     * @param array $productIds
     * @param string $method
     * @return void
     */
    private function pushJob(array $productIds, string $method = 'execute'): void
    {
        if ($this->athenaProductIndexer->isScheduled() || !$this->configHelper->getAthenaEnabledStores()) {
            return;
        }
        $this->athenaJobPublisher->publishJob(ProductSync::CODE, $method, $productIds);
    }
}
