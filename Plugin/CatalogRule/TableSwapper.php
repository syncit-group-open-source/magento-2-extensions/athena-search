<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Plugin\CatalogRule;

use Magento\CatalogRule\Model\Indexer\IndexerTableSwapper;
use Magento\Framework\Indexer\IndexerRegistry;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Model\CatalogRule\CopyMagentoRuleTables;
use Syncitgroup\AthenaSearch\Model\Indexer\Flag\CatalogRuleJobFlagFactory;
use Syncitgroup\AthenaSearch\Logger\Logger;
use Syncitgroup\AthenaSearch\Model\Indexer\ProductSync;

class TableSwapper
{
    private $athenaProductIndexer;

    private ConfigHelper $configHelper;

    private CopyMagentoRuleTables $copyMagentoRuleTables;

    private CatalogRuleJobFlagFactory $catalogRuleJobFlagFactory;

    private Logger $logger;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        ConfigHelper $configHelper,
        CopyMagentoRuleTables $copyMagentoRuleTables,
        CatalogRuleJobFlagFactory $catalogRuleJobFlagFactory,
        Logger $logger
    ) {
        $this->athenaProductIndexer = $indexerRegistry->get(ProductSync::ATHENA_PRODUCT_INDEXER);
        $this->configHelper = $configHelper;
        $this->copyMagentoRuleTables = $copyMagentoRuleTables;
        $this->catalogRuleJobFlagFactory = $catalogRuleJobFlagFactory;
        $this->logger = $logger;
    }

    /**
     * Plugin on table swap
     *
     * @param IndexerTableSwapper $indexerTableSwapper
     * @param array $originalTablesNames
     * @return array
     */
    public function beforeSwapIndexTables(IndexerTableSwapper $indexerTableSwapper, array $originalTablesNames): array
    {
        if (!$this->configHelper->getAthenaEnabledStores() || !$this->athenaProductIndexer->isScheduled()) {
            return [$originalTablesNames];
        }

        try {
            $isCopied = $this->copyMagentoRuleTables->execute();
            if ($isCopied) {
                $flag = $this->catalogRuleJobFlagFactory->create()->loadSelf();
                $flag->setState(1)->save();
            }
        } catch (\Exception $exception) {
            $this->logger->logMessage($exception);
        }

        return [$originalTablesNames];
    }
}
