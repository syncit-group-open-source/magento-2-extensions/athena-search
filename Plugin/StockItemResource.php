<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Plugin;

use Magento\CatalogInventory\Model\ResourceModel\Stock\Item as BaseStockItemResource;
use Magento\Framework\Indexer\IndexerRegistry;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Job\Publisher;
use Syncitgroup\AthenaSearch\Model\Indexer\ProductSync;

class StockItemResource
{
    private $athenaProductIndexer;

    private ConfigHelper $configHelper;

    private Publisher $athenaJobPublisher;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        ConfigHelper $configHelper,
        Publisher $athenaJobPublisher
    ) {
        $this->athenaProductIndexer = $indexerRegistry->get(ProductSync::ATHENA_PRODUCT_INDEXER);
        $this->configHelper = $configHelper;
        $this->athenaJobPublisher = $athenaJobPublisher;
    }

    /**
     * Push stock item after save event
     *
     * @param BaseStockItemResource $stockItemModel
     * @param $result
     * @param mixed $stockItem
     * @return mixed
     */
    public function afterSave(BaseStockItemResource $stockItemModel, $result, $stockItem)
    {
        $this->pushJob([$stockItem->getProductId()]);
        return $result;
    }

    /**
     * Push stock item delete
     *
     * @param BaseStockItemResource $stockItemModel
     * @param $result
     * @param mixed $stockItem
     * @return mixed
     */
    public function afterDelete(BaseStockItemResource $stockItemModel, $result, $stockItem)
    {
        $this->pushJob([$stockItem->getProductId()]);
        return $result;
    }

    /**
     * Push job to queue
     *
     * @param array $productIds
     * @return void
     */
    private function pushJob(array $productIds): void
    {
        if ($this->athenaProductIndexer->isScheduled() || empty($this->configHelper->getAthenaEnabledStores())) {
            return;
        }

        $this->athenaJobPublisher->publishJob(ProductSync::CODE, 'execute', $productIds);
    }
}
