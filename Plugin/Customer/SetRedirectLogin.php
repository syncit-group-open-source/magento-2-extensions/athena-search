<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Plugin\Customer;

use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Framework\Stdlib\CookieManagerInterface;

class SetRedirectLogin
{
    private PublicCookieMetadata $publicCookieMetadata;

    private CookieManagerInterface $cookieCustom;

    private RedirectInterface $redirect;

    public function __construct(
        PublicCookieMetadata $publicCookieMetadata,
        CookieManagerInterface $cookieCustom,
        RedirectInterface $redirect
    ) {
        $this->publicCookieMetadata = $publicCookieMetadata;
        $this->cookieCustom = $cookieCustom;
        $this->redirect = $redirect;
    }

    /**
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    public function beforeExecute()
    {
        $this->publicCookieMetadata->setPath('/');
        $referrerUrl = $this->redirect->getRefererUrl();

        $compareUrl = strpos($referrerUrl, 'customer/account/');
        if ($compareUrl === false) {
            $redirectUrl = $referrerUrl;
            $this->cookieCustom->setPublicCookie('login_redirect', $redirectUrl, $this->publicCookieMetadata);
        }
    }
}
