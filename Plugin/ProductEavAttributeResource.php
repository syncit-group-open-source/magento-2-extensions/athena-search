<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Plugin;

use Magento\Catalog\Model\ResourceModel\Eav\Attribute as BaseProductEavAttributeResource;
use Magento\Framework\Indexer\IndexerRegistry;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Job\Publisher;
use Syncitgroup\AthenaSearch\Model\Indexer\CatalogAttributeSync;

class ProductEavAttributeResource
{
    private $athenaCatalogAttributeIndexer;

    private ConfigHelper $configHelper;

    private Publisher $athenaJobPublisher;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        ConfigHelper $configHelper,
        Publisher $athenaJobPublisher
    ) {
        $this->athenaCatalogAttributeIndexer = $indexerRegistry->get(
            CatalogAttributeSync::ATHENA_CATALOG_ATTRIBUTE_INDEX
        );
        $this->configHelper = $configHelper;
        $this->athenaJobPublisher = $athenaJobPublisher;
    }

    public function afterSave(
        BaseProductEavAttributeResource $subject,
        $result
    ) {
        $this->pushJob();
        return $result;
    }

    public function afterDelete(
        BaseProductEavAttributeResource $subject,
        $result
    ) {
        $this->pushJob();
        return $result;
    }

    /**
     * Push job to queue execution
     *
     * @return void
     */
    private function pushJob(): void
    {
        if ($this->athenaCatalogAttributeIndexer->isScheduled() || !$this->configHelper->getAthenaEnabledStores()) {
            return;
        }
        $this->athenaJobPublisher->publishJob(CatalogAttributeSync::CODE, 'execute', null);
    }
}
