<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Job;

use Magento\Framework\Exception\AlreadyExistsException;
use Syncitgroup\AthenaSearch\Api\Data\JobQueueInterface;
use Syncitgroup\AthenaSearch\Model\ResourceModel\JobQueue as JobQueueResource;
use Syncitgroup\AthenaSearch\Model\ResourceModel\JobQueue\CollectionFactory as JobQueueCollectionFactory;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;

class Processor
{
    private JobQueueCollectionFactory $jobQueueCollectionFactory;

    private JobQueueResource $jobQueueResource;

    private ConfigHelper $configHelper;

    private array $processors;

    public function __construct(
        JobQueueCollectionFactory $jobQueueCollectionFactory,
        JobQueueResource $jobQueueResource,
        ConfigHelper $configHelper,
        array $processors = []
    ) {
        $this->jobQueueCollectionFactory = $jobQueueCollectionFactory;
        $this->jobQueueResource = $jobQueueResource;
        $this->configHelper = $configHelper;
        $this->processors = $processors;
    }

    /**
     * Process pending jobs
     *
     * @param string|null $jobCode
     * @return void
     * @throws AlreadyExistsException
     */
    public function process(?string $jobCode): void
    {
        if (!$this->configHelper->isDataSyncEnabled()) {
            return;
        }

        $jobQueueCollection = $this->jobQueueCollectionFactory->create()
            ->addFieldToFilter(JobQueueInterface::STATUS, JobQueueInterface::STATUS_PENDING);

        if ($jobCode) {
            $jobQueueCollection->addFieldToFilter(JobQueueInterface::MANAGER_CODE, $jobCode);
        }

        $jobQueueCollection->walk(function (JobQueueInterface $item) {
            $item->setStatus(JobQueueInterface::STATUS_IN_QUEUE);
            $this->jobQueueResource->save($item);
        });

        /** @var JobQueueInterface $job */
        foreach ($jobQueueCollection as $job) {
            $job->setStartedAt(date('Y-m-d h:i:s'));
            $this->jobQueueResource->save($job);
            $this->processors[$job->getManagerCode()]->{$job->getCallMethod()}($job);
        }
    }
}
