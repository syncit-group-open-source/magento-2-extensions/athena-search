<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Job;

use Magento\Catalog\Model\ResourceModel\Product\Relation as ProductRelation;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\AlreadyExistsException;
use Syncitgroup\AthenaSearch\Api\Data\JobQueueInterface;
use Syncitgroup\AthenaSearch\Api\Data\JobQueueInterfaceFactory;
use Syncitgroup\AthenaSearch\Model\Indexer\CatalogRulePriceSync;
use Syncitgroup\AthenaSearch\Model\Indexer\ProductSync;
use Syncitgroup\AthenaSearch\Model\ResourceModel\JobQueue as JobQueueResource;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Logger\Logger;

class Publisher
{
    private ProductRelation $productRelation;

    private ResourceConnection $resourceConnection;

    private JobQueueInterfaceFactory $jobQueueFactory;

    private JobQueueResource $jobQueueResource;

    private ConfigHelper $configHelper;

    private Logger $logger;

    public function __construct(
        ProductRelation $productRelation,
        ResourceConnection $resourceConnection,
        JobQueueInterfaceFactory $jobQueue,
        JobQueueResource $jobQueueResource,
        ConfigHelper $configHelper,
        Logger $logger
    ) {
        $this->productRelation = $productRelation;
        $this->resourceConnection = $resourceConnection;
        $this->jobQueueFactory = $jobQueue;
        $this->jobQueueResource = $jobQueueResource;
        $this->configHelper = $configHelper;
        $this->logger = $logger;
    }

    /**
     * Publish new job for syncing data to athena
     *
     * @param string $managerCode
     * @param string $callMethod
     * @param array|null $data
     * @return void
     * @throws \Exception
     */
    public function publishJob(string $managerCode, string $callMethod, ?array $data)
    {
        if (!$this->configHelper->isDataSyncEnabled()) {
            return;
        }

        if ($data) {

            $data = $this->checkIncomingProductsWithPending(
                $managerCode,
                $callMethod,
                $this->refineIncomingProductIds(
                    $managerCode,
                    $data
                )
            );

            /**
             * If we get empty array here it means all ids already have pending jobs on them
             */
            if (empty($data) || $this->hasPendingFullReindex($managerCode, $callMethod)) {
                return;
            }
        }

        /**
         * Do not schedule catalog rule price sync if there is pending full product reindex
         */
        if ($managerCode === CatalogRulePriceSync::CODE && $this->hasPendingFullReindex($managerCode, $callMethod)) {
            return;
        }

        $job = $this->jobQueueFactory->create()
            ->setStatus(JobQueueInterface::STATUS_PENDING)
            ->setManagerCode($managerCode)
            ->setCallMethod($callMethod)
            ->setDataToProcess($data);
        try {
            $jobHashIds = $this->getUniqueJobHashIds($job);
            $this->saveJob($job, $jobHashIds);
        } catch (\Exception $exception) {
            $this->logger->logMessage($exception);
        }
    }

    /**
     * Get hash job id, we need this as identifier for job combination, it's unique per combination of fields,
     * and we will skip saving new jobs that match the same id, so as not to have duplicate jobs in pending that do
     * same operation with same data set
     *
     * @param JobQueueInterface $job
     * @return array
     */
    private function getUniqueJobHashIds(JobQueueInterface $job): array
    {
        $statuses = [
            'pending' => JobQueueInterface::STATUS_PENDING,
            'queued' => JobQueueInterface::STATUS_IN_QUEUE,
            'processing' => JobQueueInterface::STATUS_PROCESSING
        ];

        foreach ($statuses as $key => $status) {
            $statuses[$key] = hash(
                'md5',
                $status
                . $job->getManagerCode()
                . $job->getCallMethod()
                // need to fetch data directly because getDataToProcess() method in model returns an array
                . $job->getData(JobQueueInterface::DATA_TO_PROCESS)
            );
        }

        return $statuses;
    }

    /**
     * Save job but only if there are no pending jobs for same method and data set
     *
     * @param JobQueueInterface $job
     * @param $jobHashIds
     * @return void
     * @throws AlreadyExistsException
     */
    private function saveJob(JobQueueInterface $job, $jobHashIds)
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()->from(JobQueueResource::TABLE_NAME)
            ->where('job_hash_id IN (?)', $jobHashIds)
            ->where('status IN (?)', [
                JobQueueInterface::STATUS_PENDING,
                JobQueueInterface::STATUS_IN_QUEUE,
                JobQueueInterface::STATUS_PROCESSING
            ])
            ->columns(JobQueueInterface::JOB_ID);

        $hasJobInWorkableStatus = $connection->fetchAll($select);

        if (empty($hasJobInWorkableStatus)) {
            $job->setJobHashId($jobHashIds['pending']);
            $this->cancelPendingReindex($job);
            $this->jobQueueResource->save($job);
        }
    }

    /**
     * Take incoming products (or full reindex) and compare with jobs already pending, update statuses accordingly
     *
     * @param string $managerCode
     * @param string $callMethod
     * @param array|null $data
     * @return array
     */
    private function checkIncomingProductsWithPending(string $managerCode, string $callMethod, array $data): array
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()->from(
            $connection->getTableName('syncit_athena_jobs_queue')
        )->where('manager_code = ?', $managerCode)
        ->where('call_method = ?', $callMethod)
        ->where('status IN(?)', [
                JobQueueInterface::STATUS_PENDING,
                JobQueueInterface::STATUS_IN_QUEUE,
                JobQueueInterface::STATUS_PROCESSING
        ])->order('data_to_process', 'DESC');

        $existingJobs = $connection->fetchAll($select);
        $inQueueEntityIds = [];
        foreach ($existingJobs as $queueEntry) {
            $entityIds = $queueEntry[JobQueueInterface::DATA_TO_PROCESS] ? array_map(
                'intval',
                explode(',', $queueEntry[JobQueueInterface::DATA_TO_PROCESS])
            ) : [];

            $inQueueEntityIds = array_merge($inQueueEntityIds, $entityIds);
        }

        foreach ($data as $key => $entityId) {
            if (in_array($entityId, $inQueueEntityIds)) {
                unset($data[$key]);
            }
        }

        return $data;
    }

    /**
     * Check incoming product ids, replace child ids with parent id
     *
     * @return array
     * @throws \Exception
     */
    public function refineIncomingProductIds(string $managerCode, array $data): ?array
    {
        if (!empty($data) && $managerCode === ProductSync::CODE) {

            $childProducts = $this->productRelation->getRelationsByChildren($data);
            $newIds = [];

            foreach ($data as $productId) {
                if (isset($childProducts[$productId])) {
                    $newIds[] = (int) array_pop($childProducts[$productId]);
                } else {
                    $newIds[] = (int) $productId;
                }
            }
            return array_unique($newIds);
        }
        return $data;
    }

    /**
     * Cancel all pending and catalog rule reindexes when full product reindex is scheduled
     *
     * @param JobQueueInterface $jobQueue
     * @return void
     */
    private function cancelPendingReindex(JobQueueInterface $jobQueue)
    {
        if (empty($jobQueue->getDataToProcess())
            && $jobQueue->getManagerCode() === ProductSync::CODE
            && $jobQueue->getCallMethod() === 'execute'
        ) {
            $connection = $this->resourceConnection->getConnection();
            $connection->update(
                $connection->getTableName('syncit_athena_jobs_queue'),
                [JobQueueInterface::STATUS => JobQueueInterface::STATUS_CANCELED],
                [
                    'status = ?' => JobQueueInterface::STATUS_PENDING,
                    'manager_code IN (?)' => [ProductSync::CODE, CatalogRulePriceSync::CODE],
                    'call_method = ?' => 'execute'
                ]
            );
        }
    }

    /**
     * Check if there is full reindex that is in pending or in queue to be executed
     *
     * @param string $managerCode
     * @param string $callMethod
     * @return bool
     */
    private function hasPendingFullReindex(string $managerCode, string $callMethod): bool
    {
        if (in_array($managerCode, [ProductSync::CODE, CatalogRulePriceSync::CODE]) && $callMethod === 'execute') {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()->from(
                $connection->getTableName('syncit_athena_jobs_queue')
            )->where('status IN (?)', [JobQueueInterface::STATUS_PENDING, JobQueueInterface::STATUS_IN_QUEUE])
                ->where('manager_code = ?', ProductSync::CODE)
                ->where('data_to_process IS NULL')
                ->limit(1);

            return (bool) $connection->fetchOne($select);
        }
        return false;
    }
}
