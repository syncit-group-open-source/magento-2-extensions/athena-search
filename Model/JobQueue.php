<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model;

use Magento\Framework\Model\AbstractModel;
use Syncitgroup\AthenaSearch\Api\Data\JobQueueInterface;
use Syncitgroup\AthenaSearch\Model\ResourceModel\JobQueue as ResourceModel;

class JobQueue extends AbstractModel implements JobQueueInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'syncit_athena_jobs_queue_model';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @inheritDoc
     */
    public function getJobId(): ?string
    {
        return $this->getData(self::JOB_ID);
    }

    /**
     * @inheritDoc
     */
    public function setStatus(int $status): self
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @inheritDoc
     */
    public function getStatus(): int
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setManagerCode(string $code): self
    {
        return $this->setData(self::MANAGER_CODE, $code);
    }

    /**
     * @inheritDoc
     */
    public function getManagerCode(): ?string
    {
        return $this->getData(self::MANAGER_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setCallMethod(string $callMethod): self
    {
        return $this->setData(self::CALL_METHOD, $callMethod);
    }

    /**
     * @inheritDoc
     */
    public function getCallMethod(): ?string
    {
        return $this->getData(self::CALL_METHOD);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): ?string
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheirtDoc
     */
    public function setStartedAt(string $startedAt): JobQueueInterface
    {
        return $this->setData(self::STARTED_AT, $startedAt);
    }

    /**
     * @inheirtDoc
     */
    public function getStartedAt(): ?string
    {
        return $this->getData(self::STARTED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setProcessedAt(string $processedAt): self
    {
        return $this->setData(self::PROCESSED_AT, $processedAt);
    }

    /**
     * @inheritDoc
     */
    public function getProcessedAt(): ?string
    {
        return $this->getData(self::PROCESSED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setJobHashId(string $jobHashId): self
    {
        return $this->setData(self::JOB_HASH_ID, $jobHashId);
    }

    /**
     * @inheritDoc
     */
    public function getJobHashId(): ?string
    {
        return $this->getData(self::JOB_HASH_ID);
    }

    /**
     * @inheritDoc
     */
    public function getRetryCount(): ?int
    {
        return (int) $this->getData(self::RETRY_COUNT);
    }

    /**
     * @inheritDoc
     */
    public function setRetryCount(int $retryCount): JobQueueInterface
    {
        return $this->setData(self::RETRY_COUNT, $retryCount);
    }

    /**
     * @inheritDoc
     */
    public function getErrorMessage(): ?string
    {
        return $this->getData(self::ERROR_MESSAGE);
    }

    /**
     * @inheritDoc
     */
    public function setErrorMessage(string $errorMessage): self
    {
        return $this->setData(self::ERROR_MESSAGE, $errorMessage);
    }

    /**
     * @inheritDoc
     */
    public function setDataToProcess(?array $data): self
    {
        return $this->setData(
            self::DATA_TO_PROCESS,
            $data ? implode(',', $data) : $data
        );
    }

    /**
     * @inheritDoc
     */
    public function getDataToProcess(): ?array
    {
        $data = $this->getData(self::DATA_TO_PROCESS);
        return $data ? array_map('intval', explode(',', $data)) : null;
    }
}
