<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model;

use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlFactory;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\Store\Api\Data\StoreInterface;
use Syncitgroup\AthenaSearch\Service\FetchRealEntityIdField;
use Syncitgroup\AthenaSearch\Service\GetAthenaProductImage;
use Syncitgroup\AthenaSearch\Helper\Config;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as AttributeCollectionFactory;

class ProductSyncObject
{
    private const MAGENTO_PRODUCT_ATTRIBUTES = [
        'name',
        'description',
        'short_description',
        'image',
        'small_image',
        'category_ids',
        'url_key',
        'tax_class_id',
        'price_type',
        'meta_title',
        'meta_description'
    ];

    private UrlFactory $urlFactory;

    private FetchRealEntityIdField $fetchRealEntityIdField;

    private ?string $entityIdField;

    private Config $configHelper;

    private GetAthenaProductImage $getAthenaProductImage;

    private AttributeCollectionFactory $attributeCollectionFactory;

    private array $filterableAttributes = [];

    public function __construct(
        UrlFactory $urlFactory,
        FetchRealEntityIdField $fetchRealEntityIdField,
        Config $configHelper,
        GetAthenaProductImage $getAthenaProductImage,
        AttributeCollectionFactory $attributeCollectionFactory
    ) {
        $this->urlFactory = $urlFactory;
        $this->fetchRealEntityIdField = $fetchRealEntityIdField;
        $this->configHelper = $configHelper;
        $this->getAthenaProductImage = $getAthenaProductImage;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
    }

    /**
     * Initialize product object that is sent to athena
     *
     * @param ProductModel $product
     * @param StoreInterface $store
     * @return array
     * @throws \Exception
     */
    public function getProductSyncData(ProductModel $product, StoreInterface $store): array
    {
        return $this->getProductData($product, $store);
    }

    /**
     * Return product attributes to select
     *
     * @return string[]
     */
    public function getProductAttributesToSelect(): array
    {
        return array_merge(
            self::MAGENTO_PRODUCT_ATTRIBUTES,
            $this->getFilterableAttributes()
        );
    }

    /**
     * Get product data to send to athena
     *
     * @param ProductModel $product
     * @param StoreInterface $store
     * @return array
     * @throws LocalizedException
     */
    public function getProductData(ProductModel $product, StoreInterface $store): array
    {
        $productData = [
            'entity_id' => (int) $product->getEntityId(),
            'sku' => $product->getSku(),
            'type_id' => $product->getTypeId(),
            'name' => $product->getName(),
            'image' => $this->getAthenaProductImage->execute($product),
            'link' => $this->getLink($product, $store),
            'categories' => $product->getCategoryIds(),
            'description' => $product->getDescription(),
            'short_description' => $product->getShortDescription(),
            'tax_class_id' => $this->getTaxClassId($product),
            'meta_title' => $product->getData('meta_title'),
            'meta_description' => $product->getData('meta_description')
        ];

        foreach ($this->filterableAttributes as $attribute) {
            if ($product->getData($attribute)) {
                $productData[$attribute] = $product->getData($attribute);
            }
        }

        return $productData;
    }

    /**
     * Get product tax class id
     *
     * @param ProductModel $product
     * @return int
     */
    private function getTaxClassId(ProductModel $product): int
    {
        switch ($product->getTypeId()) {
            case Type::TYPE_BUNDLE:
                $taxClassId = $product->getPriceType() ? $product->getTaxClassId() : 0;
                break;
            case Grouped::TYPE_CODE:
                $taxClassId = 2;
                break;
            default:
                $taxClassId = $product->getTaxClassId();
        }

        return (int) $taxClassId;
    }

    /**
     * Get product url
     *
     * @param ProductModel $product
     * @param StoreInterface $store
     * @return string
     */
    private function getLink(ProductModel $product, StoreInterface $store): string
    {
        $productUrlSuffix = $this->configHelper->getProductUrlSuffix((int) $store->getId());

        return $this->urlFactory->create()
            ->setScope($store->getId())
            ->getUrl(
                '',
                [
                    '_direct' => $product->getUrlKey(),
                    '_secure' => true
                ]
            ) . $productUrlSuffix;
    }

    /**
     * Get all filterable or filterable in search attribute codes
     *
     * @return array
     */
    private function getFilterableAttributes(): array
    {
        if (empty($this->filterableAttributes)) {
            $attributeCollection = $this->attributeCollectionFactory->create()
                ->addFieldToSelect('attribute_code')
                ->addFieldToFilter('attribute_code', ['neq' => 'price'])
                ->addFieldToFilter(
                    [
                        EavAttributeInterface::IS_FILTERABLE,
                        EavAttributeInterface::IS_FILTERABLE_IN_SEARCH
                    ],
                    [
                        1,
                        1
                    ]
                );

            $this->filterableAttributes = $attributeCollection->walk('getAttributeCode');
        }

        return $this->filterableAttributes;
    }
}
