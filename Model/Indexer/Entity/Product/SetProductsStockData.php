<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package  Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product;

use Magento\Catalog\Model\Product\Type as BaseProductType;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Downloadable\Model\Product\Type as DownloadableProductType;
use Magento\Framework\App\ResourceConnection;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\InventoryIndexer\Model\StockIndexTableNameResolverInterface;
use Magento\InventorySales\Model\ResourceModel\GetAssignedStockIdForWebsite;

class SetProductsStockData
{
    public const ATHENA_STOCK_QUANTITY_FIELD = 'salable_qty';

    public const ATHENA_IN_STOCK_FIELD = 'stock';

    private GetAssignedStockIdForWebsite $getAssignedStockIdForWebsite;

    private ResourceConnection $resourceConnection;

    private StockIndexTableNameResolverInterface $stockIndexTableNameResolver;

    private ?string $isSalableField;

    private array $simpleItems = [];

    private array $configurableItems = [];

    private array $bundleAndGroupedItems = [];

    public function __construct(
        GetAssignedStockIdForWebsite $getAssignedStockIdForWebsite,
        ResourceConnection $resourceConnection,
        StockIndexTableNameResolverInterface $stockIndexTableNameResolver
    ) {
        $this->getAssignedStockIdForWebsite = $getAssignedStockIdForWebsite;
        $this->stockIndexTableNameResolver = $stockIndexTableNameResolver;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Take products data array and set stock information
     *
     * @param array $productsData
     * @param string $websiteCode
     * @return array
     */
    public function execute(array $productsData, string $websiteCode): array
    {
        $this->initializeProductArrayMembers($productsData);

        $stockId = $this->getAssignedStockIdForWebsite->execute($websiteCode);
        $this->getStockDataWithReservations($stockId);
        $this->processBundleAndGroupedStock();
        $this->sumConfigurableProductItemStock();

        /**
         * Set totals on product data
         */
        $allStockData = $this->simpleItems + $this->configurableItems + $this->bundleAndGroupedItems;
        foreach ($productsData as $key => $data) {
            $productsData[$key][self::ATHENA_IN_STOCK_FIELD] = $allStockData[$key][self::ATHENA_IN_STOCK_FIELD];
            $productsData[$key][self::ATHENA_STOCK_QUANTITY_FIELD] =
                $allStockData[$key][self::ATHENA_STOCK_QUANTITY_FIELD];
        }

        $this->resetMembers();

        return $productsData;
    }

    /**
     * Map products to arrays
     *
     * @param array $productsData
     * @return void
     */
    private function initializeProductArrayMembers(array $productsData): void
    {
        foreach ($productsData as $productId => $product) {
            switch ($product['type_id']) {
                case BaseProductType::TYPE_SIMPLE:
                case BaseProductType::TYPE_VIRTUAL:
                case DownloadableProductType::TYPE_DOWNLOADABLE:
                case $this->getTypeGiftCard():
                    $this->simpleItems[$productId] = $product['sku'];
                    break;
                case Configurable::TYPE_CODE:
                    $this->configurableItems[$productId] = [];
                    break;
                case Grouped::TYPE_CODE:
                case BaseProductType::TYPE_BUNDLE:
                    $this->bundleAndGroupedItems[] = $productId;
                    break;
            }
        }
    }

    /**
     * Fetch related product items and find their skus, returns mapped array
     * [
     *      'main_product_id' => [
     *          'child_product_sku1','child_product_sku2'...
     *      ]
     * ]
     *
     * @return array
     */
    private function getRelatedItemSkus(): array
    {
        $productIds = array_keys($this->configurableItems);
        $productsMap = [];

        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from(['cpr' => $connection->getTableName('catalog_product_relation')], [])
            ->joinInner(
                ['cpe' => $connection->getTableName('catalog_product_entity')],
                'cpr.child_id = cpe.entity_id',
                ['cpr.parent_id','cpe.sku']
            )->where('cpr.parent_id IN(?)', $productIds);

        $result = $connection->fetchAll($select);
        foreach ($result as $productData) {
            $productsMap[$productData['parent_id']][] = $productData['sku'];
        }

        return $productsMap;
    }

    /**
     * Maps product array to it's stock quantities, array is passed by reference
     *
     * @param int|null $stockId
     * @return void
     */
    private function getStockDataWithReservations(?int $stockId)
    {
        $skuList = [];
        $productsToSkuList = $this->simpleItems + $this->getRelatedItemSkus();

        foreach ($productsToSkuList as $list) {
            is_array($list)
                ? array_push($skuList, ...array_values($list))
                : array_push($skuList, $list);
        }

        $connection = $this->resourceConnection->getConnection();
        $inventoryStockIndexTableName = $this->stockIndexTableNameResolver->execute($stockId);
        $salableField = $this->getSalableField($inventoryStockIndexTableName);

        $selectInventorySourceItem = $connection->select()->from(
            ['isi' => $inventoryStockIndexTableName],
            ['sku', 'source_quantity' => 'isi.quantity', "isi.$salableField"]
        )->where('isi.sku IN(?)', $skuList);

        $selectInventoryReservations = $connection->select()->from(
            ['ir' => $connection->getTableName('inventory_reservation')],
            ['sku', 'reservation_quantity' => 'SUM(ir.quantity)']
        )->where('ir.sku IN(?)', $skuList)
            ->where('ir.stock_id = ?', $stockId)
            ->group('sku');

        $inventorySourceItems = $connection->fetchAssoc($selectInventorySourceItem);
        $inventoryReservations = $connection->fetchAssoc($selectInventoryReservations);

        foreach ($productsToSkuList as $key => $listItem) {
            if (is_array($listItem)) {
                foreach ($listItem as $sku) {
                    if (isset($inventorySourceItems[$sku])) {
                        $this->configurableItems[$key][] = $this->calculateItemQuantity(
                            $inventorySourceItems[$sku],
                            $salableField,
                            $inventoryReservations[$sku] ?? null
                        );
                    }
                }
            } else {
                if (isset($inventorySourceItems[$listItem])) {
                    $this->simpleItems[$key] = $this->calculateItemQuantity(
                        $inventorySourceItems[$listItem],
                        $salableField,
                        $inventoryReservations[$listItem] ?? null
                    );
                }
            }
        }
    }

    /**
     * Set data for stock item on item level
     *
     * @param array $stockItems
     * @param string $salableField
     * @param array|null $reservationItems
     * @return array
     */
    private function calculateItemQuantity(array $stockItems, string $salableField, ?array $reservationItems): array
    {
        $quantity = $stockItems['source_quantity'];
        $isSalable = (int) $stockItems[$salableField];

        if (!empty($reservationItems)) {
            $quantity = $stockItems['source_quantity'] + $reservationItems['reservation_quantity'];
            $isSalable = !($quantity <= 0);
        }

        return [
            self::ATHENA_IN_STOCK_FIELD => (int) $isSalable,
            self::ATHENA_STOCK_QUANTITY_FIELD => (float) $quantity
        ];
    }

    /**
     * Loop through configurable items and sum up quantities
     *
     * @return void
     */
    private function sumConfigurableProductItemStock(): void
    {
        foreach ($this->configurableItems as $parentId => $configurableItem) {

            $isSalable = false;
            $stockQuantity = 0;

            foreach ($configurableItem as $stock) {
                /**
                 * Set in stock only once
                 */
                if (empty($isSalable) && $stock[self::ATHENA_IN_STOCK_FIELD]) {
                    $isSalable = $stock[self::ATHENA_IN_STOCK_FIELD];
                }
                $stockQuantity += $stock[self::ATHENA_STOCK_QUANTITY_FIELD];
            }

            $this->configurableItems[$parentId] += [
                self::ATHENA_IN_STOCK_FIELD => $isSalable,
                self::ATHENA_STOCK_QUANTITY_FIELD => $stockQuantity
            ];
        }

        // loop through set values and clean up, we only need summarized data
        foreach ($this->configurableItems as $key => $configurableItem) {
            $this->configurableItems[$key] = [
                self::ATHENA_IN_STOCK_FIELD => $configurableItem[self::ATHENA_IN_STOCK_FIELD],
                self::ATHENA_STOCK_QUANTITY_FIELD => $configurableItem[self::ATHENA_STOCK_QUANTITY_FIELD]
            ];
        }
    }

    /**
     * Set bundle or grouped product stock status, we do not send quantity data for them so it's set to 0
     *
     * @return void
     */
    private function processBundleAndGroupedStock(): void
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from(
                $connection->getTableName('cataloginventory_stock_status'),
                ['product_id', 'stock_status']
            )->where('product_id IN(?)', $this->bundleAndGroupedItems);

        $result = $connection->fetchAssoc($select);

        foreach ($result as $key => $item) {
            $this->bundleAndGroupedItems[$key] = [
                self::ATHENA_IN_STOCK_FIELD => $item['stock_status'],
                self::ATHENA_STOCK_QUANTITY_FIELD => 0
            ];
        }
    }

    /**
     * Reset members
     *
     * @return void
     */
    private function resetMembers(): void
    {
        $this->simpleItems = [];
        $this->configurableItems = [];
        $this->bundleAndGroupedItems = [];
    }

    /**
     * In case MSI is used with stock sources we salable field varies
     * for inventory_source_item === status
     * for inventory_stock_WEBSITEID === is_salable
     *
     * @param string $tableName
     * @return string
     */
    private function getSalableField(string $tableName): string
    {
        if (empty($this->isSalableField)) {
            $this->isSalableField = $this->resourceConnection->getTableName('inventory_source_item') === $tableName
                ? 'status'
                : 'is_salable';
        }

        return $this->isSalableField;
    }

    /**
     * In magento CE there is no giftcard by default
     *
     * @return string
     */
    public function getTypeGiftCard(): string
    {
        return (class_exists(\Magento\GiftCard\Model\Catalog\Product\Type\Giftcard::class))
            ? \Magento\GiftCard\Model\Catalog\Product\Type\Giftcard::TYPE_GIFTCARD
            : 'giftcard';
    }
}
