<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product;

use Magento\Store\Api\Data\StoreInterface;
use Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\PricesData\AbstractPricesData;

class SetPricesComposite
{
    /**
     * Temporary data, removed before dispatching to athena
     */
    public const TMP_DATA = 'tax_class_id';

    private ?array $typesBatch;

    private array $productTypePricesHandler;

    public function __construct(
        array $productTypePricesHandler = []
    ) {
        $this->productTypePricesHandler = $productTypePricesHandler;
    }

    /**
     * Set price data for product batch
     *
     * @param array $productsData
     * @param StoreInterface $store
     * @return array
     */
    public function execute(array $productsData, StoreInterface $store): array
    {
        $this->buildProductTypeBatches($productsData);

        foreach ($this->typesBatch ?? [] as $key => $batch) {
            $prices = $this->getPriceHandler($key)->getPriceData($batch, $store);
            foreach ($productsData as &$product) {
                if (isset($prices[$product['entity_id']]) && !empty($prices[$product['entity_id']])) {
                    $product += $prices[$product['entity_id']];
                    unset($product[self::TMP_DATA]);
                }
            }
        }

        $this->resetTypesBatch();

        return $productsData;
    }

    /**
     * Get type price handler instance
     *
     * @param string $typeId
     * @return AbstractPricesData
     */
    private function getPriceHandler(string $typeId): AbstractPricesData
    {
        return $this->productTypePricesHandler[$typeId];
    }

    /**
     * Divide product data into respective product type batches
     * eg: [
     *     'configurable' => [1,2,3,4,5],
     *     'simple' => [6,7,8,9]
     * ]
     *
     * @param array $productData
     * @return void
     */
    private function buildProductTypeBatches(array $productData): void
    {
        foreach ($productData as $product) {
            $this->typesBatch[$product['type_id']][$product['entity_id']] = (int) $product['tax_class_id'];
        }
    }

    /**
     * Reset types batch member
     *
     * @return void
     */
    private function resetTypesBatch(): void
    {
        $this->typesBatch = null;
    }
}
