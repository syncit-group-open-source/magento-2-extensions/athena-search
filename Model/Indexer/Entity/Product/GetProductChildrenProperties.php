<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute;
use Magento\Store\Api\Data\StoreInterface;
use Syncitgroup\AthenaSearch\Service\GetAthenaProductImage;

class GetProductChildrenProperties
{
    private array $usedProductAttributes = [];

    private GetAthenaProductImage $getAthenaProductImage;

    public function __construct(
        GetAthenaProductImage $getAthenaProductImage
    ) {
        $this->getAthenaProductImage = $getAthenaProductImage;
    }

    public function execute(ProductModel $product, StoreInterface $store): array
    {
        $childrenInfo = [];
        if ($product->getTypeId() === Configurable::TYPE_CODE) {
            $childrenInfo['configurable_options'] = $this->getConfigurableOptions($product);
            $childrenInfo['child_products'] = $this->getChildProductDetails($product, $store);
        }
        $this->usedProductAttributes = [];
        return $childrenInfo;
    }

    /**
     * Get configurable option attributes map
     *
     * @param ProductModel $product
     * @return array
     */
    private function getConfigurableOptions(ProductModel $product): array
    {
        $options = [];
        $configurableAttributes = $product->getTypeInstance()->getConfigurableAttributeCollection($product);
        /* @var $attribute Attribute */
        foreach ($configurableAttributes as $attribute) {
            $attributeCode = $attribute->getProductAttribute()->getAttributeCode();
            $this->usedProductAttributes[] = $attributeCode;
            foreach ($attribute->getOptions() ?? [] as $attributeOption) {
                $options[$attributeCode][] = $attributeOption['value_index'];
            }
        }

        return $options;
    }

    /**
     * Get configurable children data
     *
     * @param ProductModel $product
     * @param StoreInterface $store
     * @return array
     */
    private function getChildProductDetails(ProductModel $product, StoreInterface $store): array
    {
        $childProductDetails = [];
        $childProductsCollection = $product->getTypeInstance()->getUsedProductCollection($product)
            ->addAttributeToSelect($this->usedProductAttributes)
            ->addMediaGalleryData();

        foreach ($childProductsCollection as $item) {

            $childProductDetails[$item->getId()] = [
                'entity_id' => $item->getId(),
                'sku' => $item->getSku(),
                'image' => $this->getAthenaProductImage->execute($item)
            ];

            foreach ($this->usedProductAttributes as $attributeCode) {
                $childProductDetails[$item->getId()]['configurable_options'][$attributeCode]
                    = $item->getData($attributeCode);
            }
        }

        return $childProductDetails;
    }
}
