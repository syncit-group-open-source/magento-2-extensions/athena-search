<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package  Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product;

use Magento\Framework\App\ResourceConnection;
use Magento\Review\Model\Review;
use Magento\Review\Model\Review\Config as ReviewConfig;

class SetProductsReviewData
{
    private ReviewConfig $reviewConfig;

    private ResourceConnection $resourceConnection;

    public function __construct(
        ReviewConfig $reviewConfig,
        ResourceConnection $resourceConnection
    ) {
        $this->reviewConfig = $reviewConfig;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Set product review data on whole product array
     *
     * @param array $productsData
     * @param int $storeId
     * @return void
     */
    public function execute(array $productsData, int $storeId): array
    {
        $productIds = array_keys($productsData);

        if ($this->reviewConfig->isEnabled()) {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()->from(
                ['rw' => $this->resourceConnection->getTableName('review')],
                []
            )->joinInner(
                ['res' => $this->resourceConnection->getTableName('review_entity_summary')],
                'rw.entity_pk_value = res.entity_pk_value',
                []
            )->joinInner(
                ['re' => $this->resourceConnection->getTableName('review_entity')],
                'rw.entity_id = re.entity_id',
                []
            )->where('rw.entity_pk_value IN(?)', $productIds)
                ->where('rw.status_id = ?', Review::STATUS_APPROVED)
                ->where('res.store_id = ?', $storeId)
                ->columns(
                    [
                        'product_id' => 'res.entity_pk_value',
                        'reviews_count' => 'res.reviews_count',
                        'rating_summary' => 'res.rating_summary'
                    ]
                );

            $reviewData = $connection->fetchAssoc($select);

            foreach ($reviewData as $productId => $review) {

                $reviewCount = (int) $review['reviews_count'] ?? null;

                $summary = !empty($review['rating_summary']) && !empty($review['reviews_count'])
                    ? (int) $review['rating_summary'] / (int) $review['reviews_count']
                    : null;

                if (isset($productsData[$productId]['total_reviews'])) {
                    $productsData[$productId]['total_reviews'] += $reviewCount;
                    $productsData[$productId]['reviews'] += $summary;
                } else {
                    $productsData[$productId]['total_reviews'] = $reviewCount;
                    $productsData[$productId]['reviews'] = $summary;
                }
            }
        }

        return $productsData;
    }
}
