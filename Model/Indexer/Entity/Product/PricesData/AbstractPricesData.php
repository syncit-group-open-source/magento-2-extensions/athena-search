<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\PricesData;

use Magento\Customer\Model\ResourceModel\Group\CollectionFactory as CustomerGroupCollectionFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Api\Data\StoreInterface;
use Syncitgroup\AthenaSearch\Helper\ProductTaxRate;
use Magento\Tax\Model\Calculation as TaxCalculation;
use Magento\Store\Model\StoreManagerInterface;

abstract class AbstractPricesData
{
    protected const GROUP_ID = 'customer_group_id';

    /**
     * Price constants refer to athena price names, not magento
     */
    protected const PRICE_BASE = 'price';
    protected const PRICE_REGULAR = 'regular_price';
    protected const PRICE_SPECIAL = 'special_price';

    protected const GROUPS_PRICING = 'customer_groups';

    private CustomerGroupCollectionFactory $customerGroupCollectionFactory;

    private ResourceConnection $resourceConnection;

    private ProductTaxRate $productTaxRates;

    private TaxCalculation $taxCalculation;

    private StoreManagerInterface $storeManager;

    private array $customerGroupIds = [];

    public function __construct(
        CustomerGroupCollectionFactory $customerGroupCollectionFactory,
        ResourceConnection $resourceConnection,
        ProductTaxRate $productTaxRates,
        TaxCalculation $taxCalculation,
        StoreManagerInterface $storeManager
    ) {
        $this->customerGroupCollectionFactory = $customerGroupCollectionFactory;
        $this->resourceConnection = $resourceConnection;
        $this->productTaxRates = $productTaxRates;
        $this->taxCalculation = $taxCalculation;
        $this->storeManager = $storeManager;
    }

    /**
     * Get relevant product price data
     *
     * @param array $productsData
     * @param StoreInterface $store
     * @return array
     */
    public function getPriceData(
        array $productsData,
        StoreInterface $store
    ): array {
        return $this->getAllPrices(
            $this->getPricesListFromIndex(
                array_keys($productsData),
                array_unique($productsData),
                (int) $store->getWebsiteId()
            )
        );
    }

    /**
     * Get customer group ids
     *
     * @return array
     */
    protected function getCustomerGroupIds(): array
    {
        if (!$this->customerGroupIds) {
            $this->customerGroupIds = $this->customerGroupCollectionFactory->create()
                ->walk('getCustomerGroupId');
        }

        return $this->customerGroupIds;
    }

    /**
     * Load product price data only
     *
     * @param array $productIds
     * @param array $taxClassIds
     * @param int $websiteId
     * @return void
     */
    protected function getPricesListFromIndex(
        array $productIds,
        array $taxClassIds,
        int $websiteId
    ): array {

        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()->from(
            $connection->getTableName('catalog_product_index_price')
        )->where('entity_id IN(?)', $productIds)
        ->where('customer_group_id IN(?)', array_map('intval', $this->getCustomerGroupIds()))
        ->where('website_id = ?', $websiteId)
        ->where('tax_class_id IN(?)', $taxClassIds)
        ->order(new \Zend_Db_Expr('entity_id, customer_group_id ASC'));

        return $connection->fetchAll($select);
    }

    /**
     * Get base product price
     *
     * @param array $priceData
     * @return float
     */
    protected function getBasePrice(array $priceData): float
    {
        $data = [
            'price' => (float) $priceData['price'],
            'final_price' => (float) $priceData['final_price'],
            'min_price' => (float) $priceData['min_price']
        ];

        if (empty($data['price']) && empty($data['final_price'])) {
            return $data['min_price'] ?? 0;
        } else {
            if ($data['min_price'] > $data['price']) {
                return $data['min_price'];
            }
            return $data['price'] ?? 0;
        }
    }

    /**
     * Get special price, for athena this is actual product price that is lower than base product price
     *
     * @param array $priceData
     * @return float|null
     */
    protected function getSpecialPrice(array $priceData): ?float
    {
        /** @var $data */
        $data = array_filter([
            'price' => (float) $priceData['price'],
            'final_price' => (float) $priceData['final_price'],
            'min_price' => (float) $priceData['min_price'],
            'tier_price' => (float) $priceData['tier_price']
        ]);

        if (isset($data['tier_price']) && $data['tier_price'] < $data['min_price']) {
            (float) $minimalPrice = $data['tier_price'] ?? 0;
        } else {
            (float) $minimalPrice = $data['min_price'] ?? 0;
        }

        if (isset($data['final_price']) && $minimalPrice < $data['final_price']) {
            return $minimalPrice;
        }

        return null;
    }

    /**
     * Get all prices for given product price list data
     *
     * @param array $productsPriceList
     * @return array
     */
    protected function getAllPrices(
        array $productsPriceList
    ): array {

        $productPrices = [];

        foreach ($productsPriceList as $item) {
            $id = (int) $item['entity_id'];
            $taxClassId = (int) $item['tax_class_id'];
            $customerGroupId = (int) $item[self::GROUP_ID];

            if (!isset($productPrices[$id][self::PRICE_BASE])) {
                $basePrice = $this->getBasePrice($item);
                $specialPrice = $this->getSpecialPrice($item);
                $productPrices[$id] = [
                    self::PRICE_BASE => $this->applyTaxToPrice($basePrice, $taxClassId)
                ];
                if ($specialPrice) {
                    $productPrices[$id][self::PRICE_SPECIAL] = $this->applyTaxToPrice($specialPrice, $taxClassId);
                }
            }

            $basePrice = $this->getBasePrice($item);
            $specialPrice = $this->getSpecialPrice($item);

            $productPrices[$id][self::GROUPS_PRICING][$customerGroupId] = [
                self::GROUP_ID => $customerGroupId,
                self::PRICE_REGULAR => $this->applyTaxToPrice($basePrice, $taxClassId)
            ];

            if ($specialPrice) {
                $productPrices[$id][self::GROUPS_PRICING][$customerGroupId][self::PRICE_SPECIAL] =
                    $this->applyTaxToPrice($specialPrice, $taxClassId);
            }
        }

        return $productPrices;
    }

    /**
     * Get applicable tax rate and apply it to price
     *
     * @param float $price
     * @param int $taxClassId
     * @return float
     */
    protected function applyTaxToPrice($price, $taxClassId): float
    {
        $price = (float) $price;
        $rate = $this->storeManager->getStore()->getCurrentCurrencyRate();
        $convertedPrice = $price * $rate;

        $taxRate = $this->productTaxRates->getTaxRate((int) $taxClassId);
        $taxAmount = $this->taxCalculation->calcTaxAmount(
            $convertedPrice,
            $taxRate,
            true,
            true
        );

        return round($convertedPrice + $taxAmount, 2);
    }
}
