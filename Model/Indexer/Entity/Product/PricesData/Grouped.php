<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package  Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\PricesData;

use Magento\Store\Api\Data\StoreInterface;

class Grouped extends AbstractPricesData
{
    /**
     * @inheirtDoc
     */
    public function getPriceData(array $productsData, StoreInterface $store): array
    {
        $pricesList = $this->getPricesListFromIndex(
            array_keys($productsData),
            array_unique($productsData),
            (int) $store->getWebsiteId()
        );

        $prices = [];
        foreach ($pricesList as $item) {

            if (isset($prices[(int) $item['entity_id']])) {
                continue;
            }

            $prices[(int) $item['entity_id']] = [
                self::PRICE_BASE => $this->applyTaxToPrice($item['min_price'], $item['tax_class_id'])
            ];
        }

        return $prices;
    }
}
