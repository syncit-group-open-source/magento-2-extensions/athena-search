<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Indexer;

class CatalogAttributeSync extends AbstractSync
{
    public const CODE = 'catalog_attribute_sync';

    public const ATHENA_CATALOG_ATTRIBUTE_INDEX = 'athena_catalog_attribute_sync';

    /**
     * @inheirtDoc
     * @throws \Magento\Framework\Exception\InvalidArgumentException
     */
    public function execute($ids)
    {
        /**
         * Disable partial reindex for categories as athena api does not support it at the moment
         */
        $ids = null;
        $this->setCode(self::CODE);
        parent::execute($ids);
    }
}
