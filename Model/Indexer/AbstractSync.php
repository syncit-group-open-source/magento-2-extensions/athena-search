<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Indexer;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\InvalidArgumentException;
use Magento\Framework\Indexer\ActionInterface as IndexerActionInterface;
use Magento\Framework\Mview\ActionInterface as MviewActionInterface;
use Syncitgroup\AthenaSearch\Helper\Config;
use Syncitgroup\AthenaSearch\Job\Publisher;

abstract class AbstractSync implements IndexerActionInterface, MviewActionInterface, IdentityInterface
{
    private ?string $code;

    private Publisher $publisher;

    private Config $configHelper;

    public function __construct(
        Publisher $publisher,
        Config $configHelper
    ) {
        $this->publisher = $publisher;
        $this->configHelper = $configHelper;
    }

    /**
     * Push job to queue execution
     *
     * @param array|null $ids
     * @return void
     * @throws InvalidArgumentException
     */
    public function execute($ids)
    {
        if ($this->configHelper->getAthenaEnabledStores()) {

            if (!isset($this->code)) {
                throw new InvalidArgumentException(__('Job code was not set'));
            }

            $this->publisher->publishJob($this->code, 'execute', $ids ?: null);
        }
    }

    /**
     * @inheritDoc
     * @throws InvalidArgumentException
     */
    public function executeFull()
    {
        $this->execute(null);
    }

    /**
     * @inheritDoc
     * @throws InvalidArgumentException
     */
    public function executeList(array $ids)
    {
        $this->execute($ids);
    }

    /**
     * @inheritDoc
     * @throws InvalidArgumentException
     */
    public function executeRow($id)
    {
        $this->execute([$id]);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities(): array
    {
        return [];
    }

    /**
     * Set job code
     *
     * @param string $code
     * @return void
     */
    protected function setCode(string $code)
    {
        $this->code = $code;
    }
}
