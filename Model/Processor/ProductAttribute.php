<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Processor;

use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as AttributeCollection;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory as AttributeOptionCollectionFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syncitgroup\AthenaSearch\Service\DataObjectDispatcher;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Swatches\Model\Swatch;
use Magento\Swatches\Helper\Data as SwatchHelper;
use Magento\Swatches\Helper\Media as SwatchMediaHelper;

class ProductAttribute implements DataHandlerInterface
{
    private State $appState;

    private StoreManagerInterface $storeManager;

    private AttributeCollection $attributeCollectionFactory;

    private AttributeOptionCollectionFactory $attributeOptionCollectionFactory;

    private DataObjectDispatcher $dataObjectDispatcher;

    private SerializerInterface $serializer;

    private SwatchHelper $swatchHelper;

    private SwatchMediaHelper $swatchMediaHelper;

    public function __construct(
        State $appState,
        StoreManagerInterface$storeManager,
        AttributeCollection $attributeCollectionFactory,
        AttributeOptionCollectionFactory $attributeOptionCollectionFactory,
        DataObjectDispatcher $dataObjectDispatcher,
        SerializerInterface $serializer,
        SwatchHelper $swatchHelper,
        SwatchMediaHelper $swatchMediaHelper
    ) {
        $this->appState = $appState;
        $this->storeManager = $storeManager;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->attributeOptionCollectionFactory = $attributeOptionCollectionFactory;
        $this->dataObjectDispatcher = $dataObjectDispatcher;
        $this->serializer = $serializer;
        $this->swatchHelper = $swatchHelper;
        $this->swatchMediaHelper = $swatchMediaHelper;
    }

    /**
     * Push all product attributes to athena
     * @param StoreInterface $store
     * @param array|null $entityIdsToProcess
     * @return void
     * @throws LocalizedException
     */
    public function processEntitiesForStore(
        StoreInterface $store,
        ?array $entityIdsToProcess
    ): void {
        $productAttributesData = [];
        $this->setAreaCode();
        $this->storeManager->setCurrentStore($store);

        $attributesCollection = $this->attributeCollectionFactory->create()
            ->addFieldToFilter( //filter by or condition
                [
                    EavAttributeInterface::IS_FILTERABLE,
                    EavAttributeInterface::IS_FILTERABLE_IN_SEARCH
                ],
                [
                    1,
                    1
                ]
            );

        foreach ($attributesCollection as $attribute) {

            $additionalData = $attribute->getAdditionalData()
                ? $this->serializer->unserialize($attribute->getAdditionalData())
                : null;

            $isVisualSwatch = isset($additionalData[Swatch::SWATCH_INPUT_TYPE_KEY])
                && $additionalData[Swatch::SWATCH_INPUT_TYPE_KEY] === Swatch::SWATCH_INPUT_TYPE_VISUAL;

            $productAttributesData[] = [
                'id' => $attribute->getAttributeId(),
                'code' => $attribute->getAttributeCode(),
                'options' => $this->getAttributeOptions((int) $attribute->getId(), $isVisualSwatch)
            ];
        }

        $this->dataObjectDispatcher->sendData(
            $productAttributesData,
            DataObjectDispatcher::TYPE_ATTRIBUTES,
            DataObjectDispatcher::ACTION_FULL,
        );
    }

    /**
     * Get all attribute options
     *
     * @param int $attributeId
     * @param bool $isVisualSwatch
     * @return array
     */
    private function getAttributeOptions(int $attributeId, bool $isVisualSwatch): array
    {
        $attributeOptions = $this->attributeOptionCollectionFactory->create()
            ->addFieldToFilter('attribute_id', $attributeId)
            ->setStoreFilter();

        $data = [];
        $swatchOptionIds = [];

        foreach ($attributeOptions as $item) {
            if ($isVisualSwatch) {
                $swatchOptionIds[] = $item->getId();
            }
            $data[$item->getId()] = ['value' => $item->getId(), 'label' => $item->getValue()];
        }

        if ($isVisualSwatch) {
            $swatchOptions = $this->swatchHelper->getSwatchesByOptionsId($swatchOptionIds);

            foreach ($swatchOptions as $key => $swatchOption) {
                if (isset($data[$key])) {
                    if ((int) $swatchOption['type'] === Swatch::SWATCH_TYPE_VISUAL_IMAGE) {
                        $data[$key]['hashCode'] = $this->swatchMediaHelper->getSwatchAttributeImage(
                            Swatch::SWATCH_IMAGE_NAME,
                            $swatchOption['value']
                        );
                        $data[$key]['type'] = Swatch::SWATCH_TYPE_VISUAL_IMAGE;
                    } else {
                        $data[$key]['hashCode'] = $swatchOption['value'];
                        $data[$key]['type'] = Swatch::SWATCH_TYPE_VISUAL_COLOR;
                    }
                }
            }
        }

        return array_values($data);
    }

    /**
     * Check and set app state area
     *
     * @return void
     * @throws LocalizedException
     */
    private function setAreaCode()
    {
        try {
            $this->appState->getAreaCode();
        } catch (LocalizedException $exception) {
            $this->appState->setAreaCode(Area::AREA_FRONTEND);
        }
    }
}
