<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Processor;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syncitgroup\AthenaSearch\Logger\Logger;
use Syncitgroup\AthenaSearch\Model\CatalogRule\GetCurrentCatalogRuleProductIds;
use Syncitgroup\AthenaSearch\Model\CatalogRule\GetSelectForCatalogRulePricesDelta;
use Syncitgroup\AthenaSearch\Model\CatalogRule\PrepareProductIdsToWorkList;
use Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\SetPricesComposite;
use Syncitgroup\AthenaSearch\Service\DataObjectDispatcher;

class CatalogRulePrices implements DataHandlerInterface
{
    private GetSelectForCatalogRulePricesDelta $getSelectForCatalogRulePricesDelta;

    private GetCurrentCatalogRuleProductIds $getCurrentCatalogRuleProductIds;

    private PrepareProductIdsToWorkList $prepareProductIdsToWorkList;

    private SetPricesComposite $pricesComposite;

    private ProductCollectionFactory $productCollectionFactory;

    private StoreManagerInterface $storeManager;

    private DataObjectDispatcher $dataObjectDispatcher;

    private Logger $logger;

    public function __construct(
        GetSelectForCatalogRulePricesDelta $getSelectForCatalogRulePricesDelta,
        GetCurrentCatalogRuleProductIds $getCurrentCatalogRuleProductIds,
        PrepareProductIdsToWorkList $prepareProductIdsToWorkList,
        SetPricesComposite $pricesComposite,
        ProductCollectionFactory $productCollectionFactory,
        StoreManagerInterface $storeManager,
        DataObjectDispatcher $dataObjectDispatcher,
        Logger $logger
    ) {
        $this->getSelectForCatalogRulePricesDelta = $getSelectForCatalogRulePricesDelta;
        $this->getCurrentCatalogRuleProductIds = $getCurrentCatalogRuleProductIds;
        $this->prepareProductIdsToWorkList = $prepareProductIdsToWorkList;
        $this->pricesComposite = $pricesComposite;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $storeManager;
        $this->dataObjectDispatcher = $dataObjectDispatcher;
        $this->logger = $logger;
    }

    /**
     * Process catalog rule changes by dispatching updated prices to athena
     * also updates prices for product that have fallen out of catalog rule
     *
     * @param StoreInterface $store
     * @param array|null $entityIdsToProcess
     * @return void
     * @throws NoSuchEntityException
     */
    public function processEntitiesForStore(
        StoreInterface $store,
        ?array $entityIdsToProcess
    ): void {
        try {
            $productIdsChunk = array_chunk($this->getWorkListProductIds(), 1000);
        } catch (\Exception $exception) {
            $this->logger->logMessage($exception);
        }

        if (empty($productIdsChunk)) {
            return;
        }

        $this->storeManager->setCurrentStore($store);

        foreach ($productIdsChunk as $idsChunk) {
            $productCollection = $this->productCollectionFactory->create()
                ->addAttributeToSelect(['tax_class_id'])
                ->addStoreFilter($store)
                ->addFieldToFilter('entity_id', ['in' => $idsChunk]);

            $productsData = [];
            foreach ($productCollection as $product) {
                $productId = $product->getEntityId();
                $productsData[$productId] = [
                    'type_id' => $product->getTypeId(),
                    'entity_id' => $productId,
                    'tax_class_id' => $product->getTaxClassId()
                ];
            }

            $productsData = $this->pricesComposite->execute($productsData, $store);

            $this->dataObjectDispatcher->sendData(
                array_filter($productsData),
                DataObjectDispatcher::TYPE_PRICE,
                DataObjectDispatcher::ACTION_UPDATE_PRICE
            );

        }
    }

    /**
     * Get list of all product we should check prices for
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    private function getWorkListProductIds(): array
    {
        $productIdsDelta = $this->getSelectForCatalogRulePricesDelta->execute();
        $currentCatalogRuleProducts = $this->getCurrentCatalogRuleProductIds->execute();

        $allProductIds = array_unique(array_merge($productIdsDelta, $currentCatalogRuleProducts));

        return $this->prepareProductIdsToWorkList->execute($allProductIds);
    }
}
