<?php
declare(strict_types=1);

namespace Syncitgroup\AthenaSearch\Model\Processor;

use Syncitgroup\AthenaSearch\Api\Data\JobQueueInterface;
use Syncitgroup\AthenaSearch\Helper\Config;
use Syncitgroup\AthenaSearch\Model\JobManager;

class AthenaSync
{
    private Config $configHelper;

    private JobManager $jobManager;

    private DataHandlerInterface $handler;

    public function __construct(
        Config $configHelper,
        JobManager $jobManager,
        DataHandlerInterface $handler
    ) {
        $this->configHelper = $configHelper;
        $this->jobManager = $jobManager;
        $this->handler = $handler;
    }

    /**
     * Start job and work through stores
     *
     * @param JobQueueInterface $job
     * @return void
     */
    public function execute(JobQueueInterface $job)
    {
        $this->jobManager->setJobStatusTo($job, JobQueueInterface::STATUS_PROCESSING);

        try {
            $storesList = $this->configHelper->getAthenaEnabledStores();
            $entityIdsToProcess = $job->getDataToProcess();

            foreach ($storesList as $store) {
                $this->handler->processEntitiesForStore($store, $entityIdsToProcess);
            }

            $job->setProcessedAt(date('Y-m-d h:i:s'));
        } catch (\Exception $exception) {
            $this->jobManager->handleError($job, $exception);
            return;
        }

        $this->jobManager->setJobStatusTo($job, JobQueueInterface::STATUS_FINISHED);
    }

    /**
     * Start job and sync to athena deleted entities
     *
     * @param JobQueueInterface $job
     * @return void
     */
    public function pushDeletedEntity(JobQueueInterface $job)
    {
        $this->jobManager->setJobStatusTo($job, JobQueueInterface::STATUS_PROCESSING);

        try {
            $this->handler->processDeletedEntities($job->getDataToProcess());
        } catch (\Exception $exception) {
            $this->jobManager->handleError($job, $exception);
            return;
        }

        $this->jobManager->setJobStatusTo($job, JobQueueInterface::STATUS_FINISHED);
    }
}
