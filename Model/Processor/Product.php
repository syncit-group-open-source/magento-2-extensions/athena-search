<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Processor;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\GetProductChildrenProperties;
use Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\SetPricesComposite;
use Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\SetProductsReviewData;
use Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\SetProductsStockData;
use Syncitgroup\AthenaSearch\Model\ProductSyncObject;
use Syncitgroup\AthenaSearch\Service\CheckDeletedProductIds;
use Syncitgroup\AthenaSearch\Service\DataObjectDispatcher;
use Syncitgroup\AthenaSearch\Service\FilterableAttributesDataProvider;
use Syncitgroup\AthenaSearch\Service\FetchRealEntityIdField;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;

class Product implements DataHandlerInterface
{
    private State $appState;

    private ProductCollectionFactory $productCollectionFactory;

    private StoreManagerInterface $storeManager;

    private CheckDeletedProductIds $checkDeletedProductIds;

    private ProductSyncObject $productSyncObject;

    private SetPricesComposite $setPricesComposite;

    private FilterableAttributesDataProvider $filterableAttributesDataProvider;

    private GetProductChildrenProperties $getProductChildrenProperties;

    private SetProductsReviewData $setProductsReviewData;

    private SetProductsStockData $setProductsStockData;

    private DataObjectDispatcher $dataObjectDispatcher;

    private FetchRealEntityIdField $fetchRealEntityIdField;

    private ConfigHelper $configHelper;

    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        StoreManagerInterface $storeManager,
        CheckDeletedProductIds $checkDeletedProductIds,
        ProductSyncObject $productSyncObject,
        SetPricesComposite $setPricesComposite,
        FilterableAttributesDataProvider $filterableAttributesDataProvider,
        GetProductChildrenProperties $getProductChildrenProperties,
        SetProductsReviewData $setProductsReviewData,
        SetProductsStockData $setProductsStockData,
        DataObjectDispatcher $dataObjectDispatcher,
        FetchRealEntityIdField $fetchRealEntityIdField,
        State $appState,
        ConfigHelper $configHelper
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $storeManager;
        $this->checkDeletedProductIds = $checkDeletedProductIds;
        $this->productSyncObject = $productSyncObject;
        $this->getProductChildrenProperties = $getProductChildrenProperties;
        $this->setPricesComposite = $setPricesComposite;
        $this->filterableAttributesDataProvider = $filterableAttributesDataProvider;
        $this->setProductsReviewData = $setProductsReviewData;
        $this->setProductsStockData = $setProductsStockData;
        $this->dataObjectDispatcher = $dataObjectDispatcher;
        $this->fetchRealEntityIdField = $fetchRealEntityIdField;
        $this->appState = $appState;
        $this->configHelper = $configHelper;
    }

    /**
     * Get product data for given store
     *
     * @param StoreInterface $store
     * @param array|null $entityIdsToProcess
     * @return void
     * @throws LocalizedException
     * @throws \Exception
     */
    public function processEntitiesForStore(
        StoreInterface $store,
        ?array $entityIdsToProcess
    ): void {
        $this->setAreaCode();
        $this->storeManager->setCurrentStore($store);

        $productCollection = $this->productCollectionFactory->create()
            ->addAttributeToSelect($this->productSyncObject->getProductAttributesToSelect())
            ->addAttributeToFilter(ProductInterface::STATUS, Status::STATUS_ENABLED)
            ->addAttributeToFilter(ProductInterface::VISIBILITY, Visibility::VISIBILITY_BOTH)
            ->addStoreFilter($store)
            ->addCategoryIds()
            ->setPageSize($this->configHelper->getProductBatchSize())
            ->setOrder(ProductInterface::TYPE_ID, Select::SQL_ASC);

        /**
         * We filter by ids, if not empty, if no ids provided full reindex
         */
        if (!empty($entityIdsToProcess)) {
            $existingProductIds = $this->checkForDeletedItems($entityIdsToProcess);
            if (!empty($existingProductIds)) {
                $productCollection->addAttributeToFilter('entity_id', ['in' => $entityIdsToProcess]);
            } else {
                return;
            }
        }

        $currentPage = 1;
        $lastPageNumber = $productCollection->getLastPageNumber();

        while ($currentPage <= $lastPageNumber) {
            $productCollection->setCurPage($currentPage);

            $productsData = [];
            /** @var \Magento\Catalog\Model\Product $product */
            foreach ($productCollection as $product) {

                $productData = $this->productSyncObject->getProductSyncData($product, $store);

                if ($product->getTypeId() === Configurable::TYPE_CODE) {
                    $productData = $productData + $this->getProductChildrenProperties->execute($product, $store);
                }

                $productsData[
                    $product->getData($this->fetchRealEntityIdField->execute(ProductInterface::class))
                ] = $productData;

            }

            if (empty($productsData)) {
                return;
            }

            $productsData = $this->filterableAttributesDataProvider->execute(
                $productsData,
                (int) $store->getId()
            );
            $productsData = $this->setPricesComposite->execute(
                $productsData,
                $store
            );
            $productsData = $this->setProductsStockData->execute(
                $productsData,
                $store->getWebsite()->getCode()
            );
            $productsData = $this->setProductsReviewData->execute(
                $productsData,
                (int) $store->getId()
            );

            $actionType = $entityIdsToProcess
                ? DataObjectDispatcher::ACTION_UPDATE
                : DataObjectDispatcher::ACTION_FULL;

            $additionalRequestParameters = $this->getFullReindexAdditionalOptions(
                $actionType,
                $currentPage,
                $lastPageNumber
            );

            $this->dataObjectDispatcher->sendData(
                $productsData,
                DataObjectDispatcher::TYPE_PRODUCTS,
                $actionType,
                $additionalRequestParameters
            );

            $productCollection->clear();
            $currentPage++;

            if ($currentPage > $lastPageNumber) {
                break;
            }
        }
    }

    /**
     * Make an api call to push deleted product ids to athena search
     *
     * @param array $entityIds
     * @return void
     */
    public function processDeletedEntities(array $entityIds): void
    {
        $this->dataObjectDispatcher->sendData(
            $entityIds,
            DataObjectDispatcher::TYPE_PRODUCTS,
            DataObjectDispatcher::ACTION_DELETE
        );
    }

    /**
     * Check for deleted items
     *
     * @param array $entityIds
     * @return array|null
     * @throws \Exception
     */
    private function checkForDeletedItems(array $entityIds): ?array
    {
        $deletedProductIds = $this->checkDeletedProductIds->execute($entityIds);
        if ($deletedProductIds) {
            $this->processDeletedEntities($deletedProductIds);
        }

        $existingProducts = array_diff($entityIds, $deletedProductIds ?? []);
        if (empty($existingProducts)) {
            return null;
        }

        return $existingProducts;
    }

    /**
     * Check and set app state area
     *
     * @return void
     * @throws LocalizedException
     */
    private function setAreaCode()
    {
        try {
            $this->appState->getAreaCode();
        } catch (LocalizedException $exception) {
            $this->appState->setAreaCode(Area::AREA_ADMINHTML);
        }
    }

    /**
     * Options for full product reindex trigger
     *
     * @param $actionType
     * @param $currentPage
     * @param $lastPage
     * @return array
     */
    private function getFullReindexAdditionalOptions($actionType, $currentPage, $lastPage): array
    {
        $additionalOptions = [];

        if ($actionType === DataObjectDispatcher::ACTION_UPDATE) {
            return $additionalOptions;
        }

        if ($currentPage == 1 && $lastPage > 1) {
            $additionalOptions['start_reindex'] = true;
        } elseif ($currentPage == 1 && $lastPage == 1) {
            $additionalOptions['start_reindex'] = true;
            $additionalOptions['end_reindex'] = true;
        } elseif ($currentPage == $lastPage) {
            $additionalOptions['end_reindex'] = true;
        }

        return $additionalOptions;
    }
}
