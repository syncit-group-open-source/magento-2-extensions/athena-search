<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\Processor;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category as CategoryResource;
use Magento\Framework\UrlInterfaceFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syncitgroup\AthenaSearch\Service\DataObjectDispatcher;

class Category implements DataHandlerInterface
{
    private const PATH_SEPARATOR = ' > ';

    private CategoryCollectionFactory $categoryCollectionFactory;

    private CategoryResource $categoryResource;

    private UrlInterfaceFactory $urlInterfaceFactory;

    private StoreManagerInterface $storeManager;

    private DataObjectDispatcher $dataObjectDispatcher;

    public function __construct(
        CategoryCollectionFactory $categoryCollectionFactory,
        UrlInterfaceFactory $urlInterfaceFactory,
        CategoryResource $categoryResource,
        StoreManagerInterface $storeManager,
        DataObjectDispatcher $dataObjectDispatcher
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->urlInterfaceFactory = $urlInterfaceFactory;
        $this->categoryResource = $categoryResource;
        $this->storeManager = $storeManager;
        $this->dataObjectDispatcher = $dataObjectDispatcher;
    }

    /**
     * Loop through available categories on store and send data to athena
     *
     * @param StoreInterface $store
     * @param array|null $entityIdsToProcess
     * @return void
     * @throws LocalizedException
     */
    public function processEntitiesForStore(
        StoreInterface $store,
        ?array $entityIdsToProcess
    ): void {
        $this->storeManager->setCurrentStore($store);

        $categoriesCollection = $this->categoryCollectionFactory->create()
            ->addUrlRewriteToResult()
            ->addAttributeToSelect(['name', 'level', 'image', 'is_active', 'url', 'is_anchor', 'path'])
            ->addAttributeToFilter('is_active', true)
            ->addFieldToFilter('level', ['gteq' => 2])
            ->setStore($store);

        if ($entityIdsToProcess) {
            $categoriesCollection->addAttributeToFilter(['entity_id'], ['in' => $entityIdsToProcess]);
        }

        $categories = [];
        /** @var \Magento\Catalog\Model\Category $category */
        foreach ($categoriesCollection as $category) {

            $categoryUrl = $this->urlInterfaceFactory->create()->getDirectUrl(
                $category->getRequestPath(),
                [
                    '_scope' => $store->getId()
                ]
            );

            $parentCategory = $category->getParentCategory();
            $this->categoryResource->load(
                $parentCategory->setStoreId($store->getId()),
                $category->getParentId()
            );

            $categories[$category->getId()] = [
                'id' => $category->getId(),
                'parent_id' => $parentCategory->getId(),
                'parent_name' => $parentCategory->getName(),
                'level' => $category->getLevel(),
                'name' => $category->getName(),
                'image' => $category->getImageUrl() ?? null,
                'url' => $categoryUrl,
                'category_size' => $category->getProductCollection()->getSize(),
                'is_anchor' => $category->getIsAnchor(),
                'path' => $this->getCategoryPath($category)
            ];
        }

        $this->dataObjectDispatcher->sendData(
            $categories,
            DataObjectDispatcher::TYPE_CATEGORIES,
            $entityIdsToProcess ? DataObjectDispatcher::ACTION_UPDATE : DataObjectDispatcher::ACTION_FULL
        );
    }

    /**
     * Build category path
     *
     */
    private function getCategoryPath(\Magento\Catalog\Model\Category $category): string
    {
        $path = [];
        $pathIds = array_reverse(explode(',', $category->getPathInStore()));
        $categories = $category->getParentCategories();

        foreach ($pathIds as $categoryId) {
            if (isset($categories[$categoryId]) && $categories[$categoryId]->getName()) {
                $path[] = $categories[$categoryId]->getName();
            }
        }

        return implode(self::PATH_SEPARATOR, $path);
    }

    /**
     * Process deleted entities
     */
    public function processDeletedEntities(array $entityIds): void
    {
        $this->dataObjectDispatcher->sendData(
            $entityIds,
            DataObjectDispatcher::TYPE_CATEGORIES,
            DataObjectDispatcher::ACTION_DELETE
        );
    }
}
