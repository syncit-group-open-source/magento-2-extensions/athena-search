<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\CatalogRule;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ResourceConnection;
use Syncitgroup\AthenaSearch\Service\FetchRealEntityIdField;

class PrepareProductIdsToWorkList
{
    private ResourceConnection $resourceConnection;

    private FetchRealEntityIdField $fetchRealEntityIdField;

    public function __construct(
        ResourceConnection $resourceConnection,
        FetchRealEntityIdField $fetchRealEntityIdField
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->fetchRealEntityIdField = $fetchRealEntityIdField;
    }

    /**
     * Process product ids, returns parent ids if simple item has parent,
     * otherwise returns simple product id that is visible on it's own is returned as is
     *
     * @param array $productIds
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function execute(array $productIds): array
    {
        $connection = $this->resourceConnection->getConnection();
        $entityField = $this->fetchRealEntityIdField->execute(ProductInterface::class);

        $select = $connection->select()->from(
            ['cpr' => $connection->getTableName('catalog_product_relation')],
            ['child_id']
        )->join(
            ['e' => $connection->getTableName('catalog_product_entity')],
            "e.$entityField = cpr.parent_id",
            ['e.entity_id']
        )->where('cpr.child_id IN(?)', $productIds);

        $removeChildId = [];
        $parents = [];

        $query = $connection->query($select);
        while ($row = $query->fetch()) {
            $removeChildId[] = (int) $row['child_id'];
            $parents[] = (int) $row['entity_id'];
        }

        /**
         * Simple items are diff from input product ids and children product ids
         */
        $simpleItems = array_diff($productIds, $removeChildId);

        return array_unique(array_merge($simpleItems, $parents));
    }
}
