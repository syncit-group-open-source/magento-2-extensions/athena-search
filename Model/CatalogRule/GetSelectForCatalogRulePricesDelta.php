<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\CatalogRule;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Sql\UnionExpression;
use Magento\Framework\DB\Select;

/**
 * This class provides a select delta of previous version of catalogrule prices
 * and current version of catalogrule prices
 */
class GetSelectForCatalogRulePricesDelta
{
    private ResourceConnection $resourceConnection;

    private ?AdapterInterface $connection;

    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Get diff between currently applied catalog rule prices and previous version
     * returns ids of all products that matched
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function execute(): array
    {
        $this->connection = $this->resourceConnection->getConnection();

        $select = $this->getSelectDiffExpression(
            $this->connection->getTableName(
                SyncitCatalogRuleChangelogConst::TABLE_PREFIX . SyncitCatalogRuleChangelogConst::CR_TABLE
            ),
            $this->connection->getTableName(SyncitCatalogRuleChangelogConst::CR_TABLE)
        );

        $productIds = [];
        $query = $this->connection->query($select);
        while ($row = $query->fetch()) {
            $productIds[] = (int) $row['product_id'];
        }

        return $productIds;
    }

    /**
     * Return difference from both tables
     *
     * @param string $sourceTable
     * @param string $targetTable
     * @return Select
     */
    private function getSelectDiffExpression(string $sourceTable, string $targetTable): Select
    {
        $selectOriginalFields = $this->getSelectFieldsForTable(
            $sourceTable
        );
        $selectTargetFields = $this->getSelectFieldsForTable(
            $targetTable
        );

        // we use the same on condition for both selects
        $onCondition = $this->createOnCondition(
            $sourceTable,
            $targetTable,
            SyncitCatalogRuleChangelogConst::TABLE_FIELDS_MAP[$targetTable]
        );

        $selectOriginalTable = $this->connection->select()
            ->from($sourceTable, $selectOriginalFields)
            ->joinLeft(
                $targetTable,
                $onCondition,
                []
            )->where("$selectTargetFields[0] IS NULL");
        $selectTargetTable = $this->connection->select()
            ->from($targetTable, $selectTargetFields)
            ->joinLeft(
                $sourceTable,
                $onCondition,
                []
            )->where("$selectOriginalFields[0] IS NULL");

        $unionSelect = new UnionExpression(
            [
                $selectOriginalTable,
                $selectTargetTable
            ],
            Select::SQL_UNION_ALL,
            '( %s )'
        );

        return $this->connection->select()->from(['u' => $unionSelect])->distinct(true);
    }

    /**
     * Get select fields for given column set
     *
     * @param string $table
     * @param string $tableColumn
     * @return array
     */
    private function getSelectFieldsForTable(string $table, string $tableColumn = 'product_id'): array
    {
        return [sprintf('%s.%s', $table, $tableColumn)];
    }

    /**
     * Create on condition
     *
     * @param string $sourceTable
     * @param string $targetTable
     * @param array $tableColumns
     * @return string
     */
    private function createOnCondition(string $sourceTable, string $targetTable, array $tableColumns): string
    {
        $output = [];
        foreach ($tableColumns as $column) {
            $output[] = "$sourceTable.$column = $targetTable.$column";
        }
        return implode(' AND ', $output);
    }
}
