<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\CatalogRule;

/**
 * Constants related to catalog rule delta logic
 */
class SyncitCatalogRuleChangelogConst
{
    public const TABLE_PREFIX = 'syncit_';

    public const CR_TABLE = 'catalogrule_product_price';

    public const TABLE_FIELDS_MAP = [
        self::CR_TABLE => self::SYNCIT_CATALOGRULE_PRODUCT_PRICE
    ];

    private const SYNCIT_CATALOGRULE_PRODUCT_PRICE = [
        'rule_date',
        'customer_group_id',
        'product_id',
        'rule_price',
        'website_id',
        'latest_start_date',
        'earliest_end_date'
    ];
}
