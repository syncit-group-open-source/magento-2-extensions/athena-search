<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model\CatalogRule;

use Magento\Framework\App\ResourceConnection;

class GetCurrentCatalogRuleProductIds
{
    private ResourceConnection $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Return all product ids that have catalog rule price on them
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function execute(): array
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from($connection->getTableName(SyncitCatalogRuleChangelogConst::CR_TABLE), ['product_id'])
            ->distinct(true);

        $productIds = [];
        $query = $connection->query($select);
        while ($row = $query->fetch()) {
            $productIds[] = (int) $row['product_id'];
        }

        return $productIds;
    }
}
