<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */


namespace Syncitgroup\AthenaSearch\Model\CatalogRule;

use Magento\Framework\App\ResourceConnection;
use Syncitgroup\AthenaSearch\Logger\Logger;

/**
 * Service class to copy provided magento tables before they are swapped by reindex process
 */
class CopyMagentoRuleTables
{
    private ResourceConnection $resourceConnection;

    private Logger $logger;

    public function __construct(
        ResourceConnection $resourceConnection,
        Logger $logger
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->logger = $logger;
    }

    /**
     * Copy catalogrule_product_price table and data
     * @return bool
     */
    public function execute(): bool
    {
        $connection = $this->resourceConnection->getConnection();
        $tableCreated = false;
        try {
            $originalTable = $this->resourceConnection->getTableName('catalogrule_product_price');

            $changelogTableName = $this->resourceConnection->getTableName(
                SyncitCatalogRuleChangelogConst::TABLE_PREFIX . $originalTable
            );

            $connection->query(new \Zend_Db_Expr('DROP TABLE IF EXISTS ' . $changelogTableName));
            $connection->query(
                new \Zend_Db_Expr("CREATE TABLE $changelogTableName LIKE $originalTable")
            );
            $connection->query(
                new \Zend_Db_Expr("INSERT INTO $changelogTableName SELECT * FROM $originalTable")
            );
            $tableCreated = true;

        } catch (\Exception $exception) {
            $this->logger->logMessage($exception);
        }

        return $tableCreated;
    }
}
