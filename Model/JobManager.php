<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Syncitgroup
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Model;

use Syncitgroup\AthenaSearch\Api\Data\JobQueueInterface;
use Syncitgroup\AthenaSearch\Logger\Logger;
use Syncitgroup\AthenaSearch\Model\ResourceModel\JobQueue as JobQueueResource;

class JobManager
{
    private JobQueueResource $jobQueueResource;

    private Logger $logger;

    public function __construct(
        JobQueueResource $jobQueueResource,
        Logger $logger
    ) {
        $this->jobQueueResource = $jobQueueResource;
        $this->logger = $logger;
    }

    /**
     * Set job status, terminates program on exception
     *
     * @param JobQueueInterface $job
     * @param int $status
     * @return void
     */
    public function setJobStatusTo(JobQueueInterface $job, int $status)
    {
        try {
            $job->setStatus($status);
            $this->jobQueueResource->save($job);
        } catch (\Exception $exception) {
            $this->log($exception);
        }
    }

    /**
     * Handle job error and terminate
     *
     * @param JobQueueInterface $job
     * @param \Exception $exception
     * @return void
     */
    public function handleError(JobQueueInterface $job, \Exception $exception)
    {
        $this->setErrorMessage($job, $exception->getMessage());
        $this->setJobStatusTo($job, JobQueueInterface::STATUS_ERROR);
    }

    /**
     * Guess what this does
     *
     * @param JobQueueInterface $jobQueue
     * @param string $message
     * @return void
     */
    public function setErrorMessage(JobQueueInterface $jobQueue, string $message)
    {
        $jobQueue->setErrorMessage($message);
    }

    /**
     * Wrapper for logger call
     *
     * @param \Exception|string $exception
     * @return void
     */
    public function log($exception)
    {
        $this->logger->logMessage($exception);
    }
}
