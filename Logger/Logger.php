<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://www.syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs,
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright 2021 (C) SyncIt (https://www.syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://www.syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Logger;

/**
 * Class Logger | logger class
 */
class Logger extends \Monolog\Logger
{
    /**
     * Wrapper for log call
     *
     * @param \Exception|string $exception
     * @return void
     */
    public function logMessage($exception): void
    {
        if ($exception instanceof \Exception) {
            parent::log(\Monolog\Logger::INFO, $exception->getMessage());
            parent::log(\Monolog\Logger::INFO, $exception->getTraceAsString());
        } else {
            parent::log(\Monolog\Logger::INFO, $exception);
        }
    }
}
