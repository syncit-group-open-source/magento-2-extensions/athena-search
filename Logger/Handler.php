<?php
declare(strict_types=1);
/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://www.syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs,
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright 2021 (C) SyncIt (https://www.syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://www.syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Logger;

use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger as MonologLogger;
use Syncitgroup\AthenaSearch\Helper\Config;

/**
 * Class Handler | main class for logger
 */
class Handler extends Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType =  MonologLogger::INFO;

    /**
     * File name
     * @var string
     */
    public $fileName = '/var/log/athena_search_logger.log';

    private Config $configHelper;

    public function __construct(
        DriverInterface $filesystem,
        Config $configHelper,
        $filePath = null,
        $fileName = null
    ) {
        $this->configHelper = $configHelper;
        parent::__construct($filesystem, $filePath, $fileName);
    }

    /**
     * @inheirtDoc
     */
    public function write(array $record): void
    {
        if ($this->configHelper->isDebugMode()) {
            parent::write($record);
        }
    }
}
