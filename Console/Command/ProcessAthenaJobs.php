<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Console\Command;

use Magento\Framework\Exception\AlreadyExistsException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Syncitgroup\AthenaSearch\Helper\Config;
use Syncitgroup\AthenaSearch\Job\Processor;

class ProcessAthenaJobs extends Command
{
    private const OPTION_JOB_CODE = 'job-code';

    private Config $configHelper;

    private Processor $processor;

    public function __construct(
        Config $configHelper,
        Processor $processor,
        string $name = null
    ) {
        $this->configHelper = $configHelper;
        $this->processor = $processor;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('athena:jobs:run');
        $this->addOption(
            self::OPTION_JOB_CODE,
            null,
            InputOption::VALUE_OPTIONAL,
            'Process specific queue job'
        );
        $this->setDescription('Execute pending athena jobs');
        parent::configure();
    }

    /**
     * Trigger processing of athena jobs from queue by system cron job
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws AlreadyExistsException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stores = $this->configHelper->getAthenaEnabledStores();

        if (!empty($stores)) {
            $this->processor->process(
                $input->getOption(self::OPTION_JOB_CODE)
            );
        } else {
            $output->writeln('Athena search is not enabled, exiting');
        }

        return 0;
    }
}
