<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Syncitgroup\AthenaSearch\Helper\Config;
use Syncitgroup\AthenaSearch\Service\Command\SyncProductsPrices;

class SyncPrices extends Command
{
    private const SYNC_ALL_STORES = 'all_stores';

    private const PRODUCT_IDS = 'product-ids';

    private const STORE_CODE = 'store-code';

    private const CATEGORY_IDS = 'category-ids';

    private Config $configHelper;

    private SyncProductsPrices $syncProductsPrices;

    public function __construct(
        Config $configHelper,
        SyncProductsPrices $syncProductsPrices,
        string $name = null
    ) {
        $this->configHelper = $configHelper;
        $this->syncProductsPrices = $syncProductsPrices;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('athena:product-prices:sync');
        $this->setDescription('Perform synchronization of products prices data');
        $this->addOption(
            self::STORE_CODE,
            null,
            InputOption::VALUE_REQUIRED,
            'Store codes on which to apply price sync for products'
        )->addOption(
            self::PRODUCT_IDS,
            null,
            InputOption::VALUE_OPTIONAL,
            'Specify product ids for which to perform sync'
        )->addOption(
            self::CATEGORY_IDS,
            null,
            InputOption::VALUE_OPTIONAL,
            'Specify category ids for which to perform sync'
        )->setHelp(<<<EOT
The <info>athena:product-prices:sync</info>
Performs a direct update to prices on athena platform bypassing queue and indexers, <error>use at your own risk!!!</error>

<info>php bin/magento athena:product-prices:sync --store-code=store_code</info>
Sync all product prices for given store, use store code to specify which one

<info>php bin/magento athena:product-prices:sync --store-code=all_stores</info>
Sync all product prices for all stores, by passing parameter: <comment>all_stores</comment>

<info>php bin/magento athena:product-prices:sync --store-code=store_code_here --product-ids=1,2,3,4,5,6,7,8</info>
Sync prices only for specified product ids, ids need to be passed as comma separated values
EOT
        );
        parent::configure();
    }

    /**
     * CLI command description
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        [$storesToExecuteOn, $productIds, $categoryIds]
            = $this->prepareStoresAndProductData($input, $output);

        if ($storesToExecuteOn) {
            try {
                $this->syncProductsPrices->execute(
                    $storesToExecuteOn,
                    $productIds,
                    $categoryIds
                );
            } catch (\Exception $exception) {
                $output->writeln($exception->getMessage());
            }
        }
    }

    /**
     * Return store(s) and product data
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return array
     */
    private function prepareStoresAndProductData(
        InputInterface $input,
        OutputInterface $output
    ): ?array {

        $workThroughStores = [];
        $productIds = [];
        $categoryIds = [];
        $athenaEnabledStores = $this->configHelper->getAthenaEnabledStores();
        $processForStore = $input->getOption(self::STORE_CODE);

        if ($processForStore === self::SYNC_ALL_STORES) {
            $workThroughStores = $athenaEnabledStores;
        } else {
            foreach ($athenaEnabledStores ?? [] as $store) {
                if ($store->getCode() === $input->getOption(self::STORE_CODE)) {
                    $workThroughStores[$store->getId()] = $store;
                }
            }
        }

        if ($input->getOption(self::PRODUCT_IDS)) {
            $productIds = $this->mapOptionIds(
                $input->getOption(self::PRODUCT_IDS)
            );
        }

        if ($input->getOption(self::CATEGORY_IDS)) {
            $categoryIds = $this->mapOptionIds(
                $input->getOption(self::CATEGORY_IDS)
            );
        }

        if (empty($workThroughStores)) {
            $output->writeln(
                sprintf(
                    'Athena is not enabled for: <comment>%s</comment>',
                    $processForStore ? $processForStore : 'No stores enabled!'
                )
            );
            return null;
        }

        return [
            $workThroughStores,
            $productIds,
            $categoryIds
        ];
    }

    /**
     * Return array of option ids
     *
     * @param string $optionIds
     * @return array
     */
    private function mapOptionIds(string $optionIds): array
    {
        return array_map(
            'intval',
            explode(',', $optionIds)
        );
    }
}
