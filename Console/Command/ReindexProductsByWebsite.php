<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Jovana Branković <jovana.brankovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Console\Command;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Store\Model\ScopeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Syncitgroup\AthenaSearch\Helper\Config;
use Syncitgroup\AthenaSearch\Model\Processor\Product;

class ReindexProductsByWebsite extends Command
{
    private const WEBSITE_ID = 'website-id';

    private Config $configHelper;

    private Product $product;

    public function __construct(
        Config $configHelper,
        Product $product,
        string $name = null
    ) {
        $this->configHelper = $configHelper;
        $this->product = $product;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('athena:products:reindex');
        $this->addOption(
            self::WEBSITE_ID,
            null,
            InputOption::VALUE_REQUIRED,
            'Reindexing products by website id.'
        );
        $this->setDescription('Reindex products by website');
        parent::configure();
    }

    /**
     * Trigger processing of athena jobs from queue by system cron job
     *
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     * @return void
     * @throws AlreadyExistsException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $websiteEnabled = $this->configHelper->getAthenaSearchStatus(
            ScopeInterface::SCOPE_WEBSITE,
            (int)$input->getOption(self::WEBSITE_ID)
        );

        if ($websiteEnabled) {
            $storesList = $this->configHelper->getStoresList(
                $input->getOption(self::WEBSITE_ID)
            );

            foreach ($storesList as $store) {
                if ($this->configHelper->getAthenaSearchStatus(
                    ScopeInterface::SCOPE_STORE,
                    (int)$store->getId()
                )
                ) {
                    $this->product->processEntitiesForStore($store, []);
                    $output->writeln('Reindexed '.$store->getName());
                }
            }
        } else {
            $output->writeln('Athena search is not enabled, exiting');
        }
    }
}
