<?php
declare(strict_types=1);
/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service;

use GuzzleHttp\ClientFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Logger\Logger;
use Syncitgroup\AthenaSearch\Service\Api\ServiceConstantInterface;
use Syncitgroup\AthenaSearch\Serializer\Json;

class DataObjectDispatcher
{
    public const ACTION_FULL = 'index';

    public const ACTION_UPDATE = 'update';

    public const ACTION_UPDATE_PRICE = 'update_price';

    public const ACTION_DELETE = 'delete';

    public const TYPE_CATEGORIES = 'categories';

    public const TYPE_PRODUCTS = 'products';

    public const TYPE_ATTRIBUTES = 'attributes';

    public const TYPE_PRICE = 'price';

    private ConfigHelper $configHelper;

    private ClientFactory $clientFactory;

    private Logger $logger;

    private Json $serializer;

    public function __construct(
        ConfigHelper $configHelper,
        ClientFactory $clientFactory,
        Logger $logger,
        Json $serializer
    ) {
        $this->configHelper = $configHelper;
        $this->clientFactory = $clientFactory;
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    /**
     * Dispatch data to athena
     *
     * @param array $data
     * @param string $type
     * @param string $action
     * @param array $additionalRequestOptions
     * @return void
     * @throws NoSuchEntityException
     */
    public function sendData(
        array $data,
        string $type,
        string $action,
        array $additionalRequestOptions = []
    ): void {
        if (!$this->configHelper->getAthenaSearchStatus() || empty($data)) {
            return;
        }

        $dataBody = $this->getRequestDataBody($data, $type, $action, $additionalRequestOptions);

        $client = $this->clientFactory->create();

        $requestOptions = [
            'headers' => $this->configHelper->createRequestHeader(ConfigHelper::API_V2),
            'body' => $dataBody
        ];

        $response = $client->post(
            $this->getReindexEndpoint($type, $action === self::ACTION_FULL),
            $requestOptions
        );

        if ($response) {
            $statusCode = $response->getStatusCode();
            if ($statusCode !== ServiceConstantInterface::HTTP_OK) {
                $this->logger->logMessage(
                    'Request failed for data: ' . $this->serializer->serialize(
                        $data,
                        JSON_HEX_APOS|JSON_HEX_QUOT
                    )
                );
            }
        }
    }

    /**
     * Prepare body request data depending on action called
     *
     * @param array $data
     * @param string $type
     * @param string $action
     * @param array $additionalRequestOptions
     * @return string
     */
    private function getRequestDataBody(
        array $data,
        string $type,
        string $action,
        array $additionalRequestOptions
    ): string {
        $requestDataBody = [] + $additionalRequestOptions;
        $requestDataBody['token'] = $this->configHelper->getWebsiteToken();
        $requestDataBody['data'] = [];

        switch ($action) {
            case self::ACTION_FULL:
                $reindexData = [
                    'products' => [],
                    'categories' => [],
                    'attributes' => []
                ];
                $reindexData[$type] = $data;
                $requestDataBody['data'] = array_filter($reindexData);
                break;
            case self::ACTION_UPDATE:
                $requestDataBody['data'][$action] = $data;
                break;
            case self::ACTION_DELETE:
                $requestDataBody['data'][$action] = $this->_mapDeleteIds($data);
                break;
            case self::ACTION_UPDATE_PRICE:
                $requestDataBody['data'] = $data;
                break;
        }
        return $this->serializer->serialize(
            $requestDataBody,
            JSON_HEX_APOS|JSON_HEX_QUOT
        );
    }

    /**
     * Get reindex endpoint
     *
     * @param string $type
     * @param bool $productReindexFull
     * @return string
     */
    private function getReindexEndpoint(string $type, bool $productReindexFull = false): string
    {
        $reindexEndpoint = $this->configHelper->getAthenaDashboard() . ConfigHelper::API_V2;
        switch ($type) {
            case self::TYPE_CATEGORIES:
            case self::TYPE_ATTRIBUTES:
                $reindexEndpoint .= 'mapping/' . $type;
                break;
            case self::TYPE_PRODUCTS:
                $productReindexFull
                    ? $reindexEndpoint .= 'products/rawData'
                    : $reindexEndpoint .= 'open-api/reindex/partial/async';
                break;
            case self::TYPE_PRICE:
                $reindexEndpoint .= 'reindex/price/async';
                break;
        }

        return $reindexEndpoint;
    }

    /**
     * Map entity_id for delete
     *
     * @param array $data
     * @return array
     */
    private function _mapDeleteIds(array $data): array
    {
        return array_map(fn($value) => ['entity_id' => $value], $data);
    }
}
