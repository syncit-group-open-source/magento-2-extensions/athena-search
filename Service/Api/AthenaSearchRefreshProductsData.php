<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Api;

use Syncitgroup\AthenaSearch\Api\AthenaSearchRefreshProductsDataInterface;
use Syncitgroup\AthenaSearch\Model\Indexer\ProductSync;

class AthenaSearchRefreshProductsData implements AthenaSearchRefreshProductsDataInterface, ServiceConstantInterface
{

    private ProductSync $productSync;

    public function __construct(
        ProductSync $productSync
    ) {
        $this->productSync = $productSync;
    }

    /**
     * @inheritDoc
     */
    public function execute(): array
    {
        try {
            $this->productSync->execute(null);
            $message = self::MSG_SUCCESS;
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
        }

        return [
            ['message' => $message]
        ];
    }
}
