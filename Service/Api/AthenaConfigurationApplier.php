<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Api;

use Syncitgroup\AthenaSearch\Api\AthenaConfigurationApplierInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\Exception\ConfigurationMismatchException;

class AthenaConfigurationApplier implements AthenaConfigurationApplierInterface, ServiceConstantInterface
{
    private WriterInterface $configWriter;

    private CacheManager $cacheManager;

    public function __construct(
        WriterInterface $configWriter,
        CacheManager $cacheManager
    ) {
        $this->configWriter = $configWriter;
        $this->cacheManager = $cacheManager;
    }

    /**
     * @inheritDoc
     */
    public function setConfigValues(array $configs): array
    {
        try {
            foreach ($configs as $key => $value) {
                $this->configWriter->save(self::SEARCH_CONFIG_PATH . $key, $value);
            }

            $this->cacheManager->clean(self::CLEAN_CACHE_AFTER_CONFIG_SAVE);

            return [
                ['message' => self::MSG_SUCCESS]
            ];
        } catch (\Exception $exception) {
            return [
                ['message' => $exception->getMessage()]
            ];
        }
    }

    /**
     * @inheritDoc
     */
    public function setAutocompleteType(string $config): array
    {
        if (in_array($config, ['0', '1'])) {
            return $this->setConfigValues(['layer' => $config]);
        } else {
            throw new ConfigurationMismatchException(__('Config is not correct!'), null, self::HTTP_NOT_AUTH);
        }
    }
}
