<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */
namespace Syncitgroup\AthenaSearch\Service\Api;

interface ServiceConstantInterface
{
    const HTTP_OK = 200;

    const HTTP_ERR = 404;

    const HTTP_NOT_AUTH = 401;

    const MSG_SUCCESS = 'success';

    const SEARCH_CONFIG_PATH = 'athena_search/general/';

    const STYLE_CONFIG_PATH = 'athena_search/styles/';

    public const CLEAN_CACHE_AFTER_CONFIG_SAVE = [
        \Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER,
        \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER,
        \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER
    ];
}
