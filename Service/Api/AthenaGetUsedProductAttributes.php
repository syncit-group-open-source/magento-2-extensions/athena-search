<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Api;

use Syncitgroup\AthenaSearch\Api\AthenaGetUsedProductAttributesInterface;
use Syncitgroup\AthenaSearch\Model\Indexer\CatalogAttributeSync;

class AthenaGetUsedProductAttributes implements AthenaGetUsedProductAttributesInterface, ServiceConstantInterface
{
    private CatalogAttributeSync $catalogAttributeSync;

    public function __construct(
        CatalogAttributeSync $catalogAttributeSync
    ) {
        $this->catalogAttributeSync = $catalogAttributeSync;
    }

    /**
     * @inheritDoc
     */
    public function getAttributes(): array
    {
        try {
            $this->catalogAttributeSync->execute(null);
            $message = self::MSG_SUCCESS;
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
        }

        return [
            ['message' => $message]
        ];
    }
}
