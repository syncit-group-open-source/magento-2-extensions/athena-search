<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Jovana Brankovic <jovana.brankovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Api;

use Magento\Store\Model\StoreManagerInterface as Store;
use Syncitgroup\AthenaSearch\Api\RequestProductDataInterface;
use Syncitgroup\AthenaSearch\Model\Processor\Product;
use Syncitgroup\AthenaSearch\Serializer\Json;
use Syncitgroup\AthenaSearch\Service\Api\Mock\DataObjectDispatcherMock;
use Syncitgroup\AthenaSearch\Logger\Logger;

/**
 * Class RequestProductData
 */

class RequestProductData implements RequestProductDataInterface
{
    private Logger $logger;

    private Product $product;

    private Store $store;

    private DataObjectDispatcherMock $dataObjectDispatcherMock;

    private Json $serializer;

    public function __construct(
        Logger $logger,
        Product $product,
        Store $store,
        DataObjectDispatcherMock $dataObjectDispatcherMock,
        Json $serializer
    ) {
        $this->logger = $logger;
        $this->product = $product;
        $this->store = $store;
        $this->dataObjectDispatcherMock = $dataObjectDispatcherMock;
        $this->serializer = $serializer;
    }

    /**
     * @param string $storeId
     * @param string $productIds
     * @return string
     */
    public function getPost(string $storeId, string $productIds): string
    {
        try {
            $entityIdsToProcess = array_slice(explode(',', $productIds), 0, 10);

            $property = new \ReflectionProperty(
                $this->product,
                'dataObjectDispatcher'
            );
            $property->setAccessible(true);
            $property->setValue($this->product, $this->dataObjectDispatcherMock);

            $this->product->processEntitiesForStore(
                $this->store->getStore($storeId),
                $entityIdsToProcess
            );

            $response = $this->dataObjectDispatcherMock->getData();

        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
            $this->logger->logMessage($e->getMessage());
        }

        return $this->serializer->serialize($response);
    }
}
