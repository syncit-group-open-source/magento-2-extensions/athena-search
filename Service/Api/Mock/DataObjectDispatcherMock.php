<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Jovana Brankovic <jovana.brankovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Api\Mock;

use Syncitgroup\AthenaSearch\Service\DataObjectDispatcher;

class DataObjectDispatcherMock extends DataObjectDispatcher
{
    private array $data = [];

    /**
     * Dispatch data to athena
     *
     * @param array $data
     * @param string $type
     * @param string $action
     * @param array $additionalRequestOptions
     * @return void
     */
    public function sendData(
        array $data,
        string $type,
        string $action,
        array $additionalRequestOptions = []
    ): void {
        if (empty($this->data)) {
            $this->data = $data;
        }
    }

    /**
     * Return $data to testApi
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
