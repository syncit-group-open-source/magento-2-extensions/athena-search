<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Api;

use Magento\Framework\Exception\InvalidArgumentException;
use Syncitgroup\AthenaSearch\Api\AthenaSearchStylesManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Webapi\Rest\Response;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\Serialize\SerializerInterface;

class AthenaSearchStylesManager implements AthenaSearchStylesManagerInterface, ServiceConstantInterface
{
    private const REMOVE_SUFFIX = '.css';

    private const TEXT_FIELD_MAX_SIZE = 65535;

    private ScopeConfigInterface $scopeConfig;

    private WriterInterface $configWriter;

    private Response $response;

    private SerializerInterface $serializer;

    private CacheManager $cacheManager;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        WriterInterface $configWriter,
        Response $response,
        SerializerInterface $serializer,
        CacheManager $cacheManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->response = $response;
        $this->serializer = $serializer;
        $this->cacheManager = $cacheManager;
    }

    /**
     * @inheirtDoc
     */
    public function setStyle(string $content, string $path)
    {
        $response = ['message' => ''];
        $styleType = str_replace(self::REMOVE_SUFFIX, '', $path);
        try {
            if (strlen($content) > self::TEXT_FIELD_MAX_SIZE) {
                throw new InvalidArgumentException(__('Content field exceeds maximum size'));
            }

            $shouldCleanCache = $this->scopeConfig->getValue(self::STYLE_CONFIG_PATH . $styleType) !== $content;

            $this->configWriter->save(
                self::STYLE_CONFIG_PATH . $styleType,
                $content
            );
            $response['message'] = self::MSG_SUCCESS;

            if ($shouldCleanCache) {
                $this->cacheManager->clean(self::CLEAN_CACHE_AFTER_CONFIG_SAVE);
            }

        } catch (\Exception $exception) {
            $response['message'] = $exception->getMessage();
        }

        $this->response->setBody($this->serializer->serialize($response));
        $this->response->setHttpResponseCode(
            $response['message'] === self::MSG_SUCCESS
                ? self::HTTP_OK
                : self::HTTP_ERR
        );
        $this->response->sendResponse();
    }

    /**
     * @inheirtDoc
     */
    public function getStyle(string $styleType): string
    {
        return (string) $this->scopeConfig->getValue(self::STYLE_CONFIG_PATH . $styleType);
    }
}
