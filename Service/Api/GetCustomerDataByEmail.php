<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Api;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\ResourceModel\Customer as CustomerResource;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Syncitgroup\AthenaSearch\Api\GetCustomerDataByEmailInterface;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Webapi\Rest\Response;
use Magento\Framework\Serialize\SerializerInterface;

class GetCustomerDataByEmail implements GetCustomerDataByEmailInterface, ServiceConstantInterface
{
    private CustomerResource $customerResource;

    private CustomerFactory $customerFactory;

    private CustomerMetadataInterface $customerMetadata;

    private StoreManagerInterface $storeManager;

    private GroupRepositoryInterface $groupRepository;

    private Response $response;

    private SerializerInterface $serializer;

    public function __construct(
        CustomerResource $customerResource,
        CustomerFactory $customerFactory,
        CustomerMetadataInterface $customerMetadata,
        StoreManagerInterface $storeManager,
        GroupRepositoryInterface $groupRepository,
        Response $response,
        SerializerInterface $serializer
    ) {
        $this->customerResource = $customerResource;
        $this->customerFactory = $customerFactory;
        $this->customerMetadata = $customerMetadata;
        $this->storeManager = $storeManager;
        $this->groupRepository = $groupRepository;
        $this->response = $response;
        $this->serializer = $serializer;
    }

    /**
     * @inheritDoc
     */
    public function getCustomerByEmail(string $email): void
    {
        try {
            $customer = $this->getCustomerModel($email);

            $result = [];
            $result['id'] = $customer->getId();
            $result['first_name'] = $customer->getFirstname();
            $result['last_name'] = $customer->getLastname();
            $result['email'] = $customer->getEmail();
            $result['date_of_birth'] = $customer->getDob();
            $result['gender'] = $this->getGenderLabel((int) $customer->getGender());
            $result['group'] = $this->groupRepository->getById($customer->getId())->getCode();

            $defaultAddress = $customer->getDefaultBillingAddress();
            if ($defaultAddress) {
                $result['city'] = $defaultAddress->getCity();
                $result['country'] = $defaultAddress->getCountry();
            }
        } catch (\Exception $exception) {
            $result = ['message' => $exception->getMessage()];
        }

        $this->response->setHttpResponseCode(self::HTTP_OK);
        $this->response->setBody($this->serializer->serialize($result));
        $this->response->sendResponse();
    }

    /**
     * Get customer model by email
     *
     * @param string $email
     * @return Customer
     * @throws NoSuchEntityException|LocalizedException
     */
    private function getCustomerModel(string $email): Customer
    {
        $customer = $this->customerFactory->create()
            ->setWebsiteId(
                $this->storeManager->getStore()->getWebsiteId()
            );
        $this->customerResource->loadByEmail($customer, $email);

        if (empty($customer->getId())) {
            throw new NoSuchEntityException(__('No such customer with email: %1', $email));
        }

        return $customer;
    }

    /**
     * Get customer gender label
     *
     * @param int $genderOptionId
     * @return string
     */
    private function getGenderLabel(int $genderOptionId): string
    {
        try {
            $genderAttributeOptions = $this->customerMetadata->getAttributeMetadata(
                CustomerInterface::GENDER
            )->getOptions();

            return trim($genderAttributeOptions[$genderOptionId]->getLabel());
        } catch (\Exception $exception) {
            return '';
        }
    }
}
