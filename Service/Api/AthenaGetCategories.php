<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Api;

use Syncitgroup\AthenaSearch\Api\AthenaGetCategoriesInterface;
use Syncitgroup\AthenaSearch\Model\Indexer\CategorySync;

class AthenaGetCategories implements AthenaGetCategoriesInterface, ServiceConstantInterface
{
    private CategorySync $categorySync;

    public function __construct(
        CategorySync $categorySync
    ) {
        $this->categorySync = $categorySync;
    }

    /**
     * @inheirtDoc
     */
    public function getCategories(): array
    {
        try {
            $this->categorySync->execute(null);
            $message = self::MSG_SUCCESS;
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
        }

        return [
            ['message' => $message]
        ];
    }
}
