<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Jovana Brankovic <jovana.brankovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Api;

use Magento\Store\Model\StoreManagerInterface as Store;
use Syncitgroup\AthenaSearch\Api\RequestCategoryDataInterface;
use Syncitgroup\AthenaSearch\Model\Processor\Category;
use Syncitgroup\AthenaSearch\Serializer\Json;
use Syncitgroup\AthenaSearch\Service\Api\Mock\DataObjectDispatcherMock;
use Syncitgroup\AthenaSearch\Logger\Logger;

/**
 * Class RequestCategoryData
 */
class RequestCategoryData implements RequestCategoryDataInterface
{
    private Logger $logger;

    private Category $category;

    private Store $store;

    private DataObjectDispatcherMock $dataObjectDispatcherMock;

    private Json $serializer;

    public function __construct(
        Logger $logger,
        Category $category,
        Store $store,
        DataObjectDispatcherMock $dataObjectDispatcherMock,
        Json $serializer
    ) {
        $this->logger = $logger;
        $this->category = $category;
        $this->store = $store;
        $this->dataObjectDispatcherMock = $dataObjectDispatcherMock;
        $this->serializer = $serializer;
    }

    /**
     * @param string $storeId
     * @return string
     */
    public function getPost(string $storeId): string
    {
        try {

            $property = new \ReflectionProperty(
                $this->category,
                'dataObjectDispatcher'
            );
            $property->setAccessible(true);
            $property->setValue($this->category, $this->dataObjectDispatcherMock);

            $this->category->processEntitiesForStore(
                $this->store->getStore($storeId),
                null
            );

            $response = $this->dataObjectDispatcherMock->getData();

        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
            $this->logger->logMessage($e->getMessage());
        }

        return $this->serializer->serialize($response);
    }
}
