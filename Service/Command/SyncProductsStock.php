<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Jovana Branković <jovana.brankovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Command;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\DB\Select;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syncitgroup\AthenaSearch\Service\DataObjectDispatcher;
use Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\SetProductsStockData;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;

class SyncProductsStock
{
    private ProductCollectionFactory $productCollectionFactory;

    private SetProductsStockData $setProductsStockData;

    private StoreManagerInterface $storeManager;

    private DataObjectDispatcher $dataObjectDispatcher;

    private ConfigHelper $configHelper;

    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        SetProductsStockData $setProductsStockData,
        StoreManagerInterface $storeManager,
        DataObjectDispatcher $dataObjectDispatcher,
        ConfigHelper $configHelper
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->setProductsStockData = $setProductsStockData;
        $this->storeManager = $storeManager;
        $this->dataObjectDispatcher = $dataObjectDispatcher;
        $this->configHelper = $configHelper;
    }

    /**
     * Perform product stock sync to athena
     *
     * @param StoreInterface[] $stores
     * @param array $productIds
     * @return void
     * @throws NoSuchEntityException
     */
    public function execute(array $stores, array $productIds = [])
    {
        foreach ($stores as $store) {

            $this->storeManager->setCurrentStore($store);

            $productCollection = $this->productCollectionFactory->create()
                ->addAttributeToSelect(['*'])
                ->addAttributeToFilter(
                    ProductInterface::STATUS,
                    Status::STATUS_ENABLED
                )
                ->addAttributeToFilter(
                    ProductInterface::VISIBILITY,
                    Visibility::VISIBILITY_BOTH
                )
                ->setPageSize($this->configHelper->getProductBatchSize())
                ->setOrder(ProductInterface::TYPE_ID, Select::SQL_ASC);

            if ($productIds) {
                $productCollection->addAttributeToFilter(
                    'entity_id',
                    ['in' => $productIds]
                );
            }

            $currentPage = 1;
            $lastPageNumber = $productCollection->getLastPageNumber();

            while ($currentPage <= $lastPageNumber) {

                $productCollection->setCurPage($currentPage);
                $productsData = [];

                /** @var \Magento\Catalog\Model\Product $product */
                foreach ($productCollection as $product) {
                    $productsData[$product->getEntityId()] = [
                        'type_id' => $product->getTypeId(),
                        'entity_id' => $product->getEntityId(),
                        'sku' => $product->getSku(),
                    ];
                }

                $productsData = $this->setProductsStockData->execute(
                    $productsData,
                    $store->getWebsite()->getCode()
                );

                $this->dataObjectDispatcher->sendData(
                    array_filter($productsData),
                    DataObjectDispatcher::TYPE_PRICE,
                    DataObjectDispatcher::ACTION_UPDATE_PRICE
                );

                $productCollection->clear();
                $currentPage++;

                if ($currentPage > $lastPageNumber) {
                    break;
                }
            }
        }
    }
}
