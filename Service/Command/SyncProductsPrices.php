<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service\Command;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\DB\Select;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syncitgroup\AthenaSearch\Model\Indexer\Entity\Product\SetPricesComposite;
use Syncitgroup\AthenaSearch\Service\DataObjectDispatcher;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;

class SyncProductsPrices
{
    private ProductCollectionFactory $productCollectionFactory;

    private StoreManagerInterface $storeManager;

    private SetPricesComposite $pricesComposite;

    private DataObjectDispatcher $dataObjectDispatcher;

    private ConfigHelper $configHelper;

    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        StoreManagerInterface $storeManager,
        SetPricesComposite $setPricesComposite,
        DataObjectDispatcher $dataObjectDispatcher,
        ConfigHelper $configHelper
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $storeManager;
        $this->pricesComposite = $setPricesComposite;
        $this->dataObjectDispatcher = $dataObjectDispatcher;
        $this->configHelper = $configHelper;
    }

    /**
     * Perform syncing of price data
     *
     * @param StoreInterface[] $stores
     * @param array $productIds
     * @param array $categoryIds
     * @return void
     * @throws NoSuchEntityException
     */
    public function execute(
        array $stores,
        array $productIds = [],
        array $categoryIds = []
    ) {
        foreach ($stores as $store) {

            $this->storeManager->setCurrentStore($store);

            $productCollection = $this->productCollectionFactory->create()
                ->addAttributeToSelect(['tax_class_id'])
                ->addAttributeToFilter(
                    ProductInterface::STATUS,
                    Status::STATUS_ENABLED
                )
                ->addAttributeToFilter(
                    ProductInterface::VISIBILITY,
                    Visibility::VISIBILITY_BOTH
                )
                ->setPageSize($this->configHelper->getProductBatchSize())
                ->setOrder(ProductInterface::TYPE_ID, Select::SQL_ASC);

            if ($productIds) {
                $productCollection->addAttributeToFilter(
                    'entity_id',
                    ['in' => $productIds]
                );
            }

            if ($categoryIds) {
                $productCollection->addCategoriesFilter(
                    ['in' => $categoryIds]
                );
            }

            $currentPage = 1;
            $lastPageNumber = $productCollection->getLastPageNumber();

            while ($currentPage <= $lastPageNumber) {

                $productCollection->setCurPage($currentPage);
                $productsData = [];

                /** @var \Magento\Catalog\Model\Product $product */
                foreach ($productCollection as $product) {
                    $productsData[$product->getEntityId()] = [
                        'type_id' => $product->getTypeId(),
                        'entity_id' => $product->getEntityId(),
                        'tax_class_id' => $product->getTaxClassId()
                    ];
                }

                $productsData = $this->pricesComposite->execute(
                    $productsData,
                    $store
                );

                $this->dataObjectDispatcher->sendData(
                    array_filter($productsData),
                    DataObjectDispatcher::TYPE_PRICE,
                    DataObjectDispatcher::ACTION_UPDATE_PRICE
                );

                $productCollection->clear();
                $currentPage++;

                if ($currentPage > $lastPageNumber) {
                    break;
                }
            }
        }
    }
}
