<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\ResourceConnection;

class FilterableAttributesDataProvider
{
    private ResourceConnection $resourceConnection;

    private FetchRealEntityIdField $fetchRealEntityIdField;

    private ?array $filterableAttributes;

    public function __construct(
        ResourceConnection $resourceConnection,
        FetchRealEntityIdField $fetchRealEntityIdField
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->fetchRealEntityIdField = $fetchRealEntityIdField;
    }

    /**
     * Set product filterable attributes data
     *
     * @return void
     */
    public function execute(array $productsData, int $storeId): array
    {
        $this->initFilterableAttributes();

        $configurableProductIds = [];

        foreach ($productsData as $entityId => $productData) {
            if ($productData['type_id'] === Configurable::TYPE_CODE) {
                $configurableProductIds[] = $entityId;
            }
        }

        if (empty($configurableProductIds) || empty($this->filterableAttributes)) {
            return $productsData;
        }

        $connection = $this->resourceConnection->getConnection();
        $entityIdField = $this->fetchRealEntityIdField->execute(ProductInterface::class);

        $select = $connection->select()->from(
            ['cpei' => $connection->getTableName('catalog_product_entity_int')],
            [
                'cpsl.parent_id',
                'cpei.attribute_id',
                'option_ids' => new \Zend_Db_Expr('GROUP_CONCAT(DISTINCT cpei.value SEPARATOR \',\')')
            ]
        )->joinInner(
            ['cpsl' => $connection->getTableName('catalog_product_super_link')],
            "cpsl.product_id = cpei.$entityIdField",
            []
        )->where('cpei.value IS NOT NULL')
        ->where('cpei.attribute_id IN (?)', array_keys($this->filterableAttributes))
        ->where('cpei.store_id IN(?)', [0, $storeId])
        ->where('cpsl.parent_id IN (?)', $configurableProductIds)
        ->group(['cpei.attribute_id', 'cpsl.parent_id']);

        $result = $connection->fetchAll($select);

        foreach ($result as $option) {
            $attributeCode = $this->filterableAttributes[$option['attribute_id']]['attribute_code'];
            $productsData[(int) $option['parent_id']][$attributeCode] = $option['option_ids'];
        }

        return $productsData;
    }

    /**
     * Initialize filterable attribute data
     *
     * @return void
     */
    private function initFilterableAttributes()
    {
        if (!isset($this->filterableAttributes)) {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()->from(
                ['cea' => $connection->getTableName('catalog_eav_attribute')],
                []
            )->joinInner(
                ['ea' => $connection->getTableName('eav_attribute')],
                'cea.attribute_id = ea.attribute_id',
                ['ea.attribute_id', 'ea.attribute_code']
            )->where('ea.entity_type_id = ?', CategorySetup::CATALOG_PRODUCT_ENTITY_TYPE_ID)
                ->where('ea.backend_type = ?', 'int')
                ->where('cea.is_filterable = ? OR cea.is_filterable_in_search = ?', 1);

            $this->filterableAttributes = $connection->fetchAssoc($select);
        }
    }
}
