<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Jovana Brankovic <jovana.brankovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service;

use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Image\ParamsBuilder;
use Magento\Catalog\Model\View\Asset\ImageFactory as AssetImageFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\ConfigInterface;

class GetAthenaProductImage
{
    private const ATHENA_SEARCH_IMAGE = 'athena_product_image';

    private const BASE_MAGENTO_PLACEHOLDER = 'images/product/placeholder/image.jpg';

    private ConfigInterface $presentationConfig;

    private ParamsBuilder $imageParamsBuilder;

    private AssetImageFactory $viewAssetImageFactory;

    private ScopeConfigInterface $scopeConfig;

    private ?string $placeholderImagePath;

    public function __construct(
        ConfigInterface $presentationConfig,
        ParamsBuilder $imageParamsBuilder,
        AssetImageFactory $viewAssetImageFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->presentationConfig = $presentationConfig;
        $this->imageParamsBuilder = $imageParamsBuilder;
        $this->viewAssetImageFactory = $viewAssetImageFactory;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     *  Get image url from product
     *
     * @param Product $product
     * @param string $module
     * @param string $mediaId
     * @param string $imageCode
     * @return string
     * @throws LocalizedException
     */
    public function execute(
        Product $product,
        string $module = 'Syncitgroup_AthenaSearch',
        string $mediaId = self::ATHENA_SEARCH_IMAGE,
        string $imageCode = 'image'
    ): string {
        $imageConfig = $this->presentationConfig->getViewConfig(
            [
                'area' => Area::AREA_FRONTEND
            ]
        )->getMediaAttributes(
            $module,
            ImageHelper::MEDIA_TYPE_CONFIG_NODE,
            $mediaId
        );

        $imageMiscParams = $this->imageParamsBuilder->build($imageConfig);
        $originalFilePath = $product->getData($imageCode);

        $imageAsset = $this->viewAssetImageFactory->create(
            [
                'miscParams' => $imageMiscParams,
                'filePath' => $originalFilePath ?? $this->getPlaceholderImageUrl(),
            ]
        );

        return preg_replace('(^https?:)', '', $imageAsset->getUrl());
    }

    /**
     * Get placeholder path
     *
     * @return string
     */
    public function getPlaceholderImageUrl(): string
    {
        if (!isset($this->placeholderImagePath)) {
            $customPlaceholder = $this->scopeConfig->getValue(
                'catalog/placeholder/image_placeholder'
            );

            if (!empty($customPlaceholder)) {
                $this->placeholderImagePath = $customPlaceholder;
            } else {
                $this->placeholderImagePath = self::BASE_MAGENTO_PLACEHOLDER;
            }
        }

        return $this->placeholderImagePath;
    }
}
