<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service;

use Magento\Framework\App\ResourceConnection;

class CheckDeletedProductIds
{
    private ResourceConnection $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection,
        FetchRealEntityIdField $fetchRealEntityIdField
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Check for removed product ids
     *
     * @param array $entityIds
     * @return array|null
     * @throws \Exception
     */
    public function execute(array $entityIds): ?array
    {
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()->from(
            ['cpe' => $connection->getTableName('catalog_product_entity')],
            ['cpe.entity_id']
        )->where('cpe.entity_id IN (?)', $entityIds);

        $existingIds = array_keys($connection->fetchAssoc($select));

        $deletedIds = array_diff($entityIds, $existingIds);

        return !empty($deletedIds) ? $deletedIds : null;
    }
}
