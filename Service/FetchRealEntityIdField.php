<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Service;

use Magento\Framework\EntityManager\MetadataPool;

class FetchRealEntityIdField
{
    private ?array $linkedField;

    private MetadataPool $metadataPool;

    public function __construct(
        MetadataPool $metadataPool
    ) {
        $this->metadataPool = $metadataPool;
    }

    /**
     * Api correct field used for storing entity id
     *
     * @param string $entityType
     * @return string
     * @throws \Exception
     */
    public function execute(string $entityType): string
    {
        if (!isset($this->linkedField[$entityType])) {
            $this->linkedField[$entityType] = $this->metadataPool->getMetadata($entityType)->getLinkField();
        }

        return $this->linkedField[$entityType];
    }
}
