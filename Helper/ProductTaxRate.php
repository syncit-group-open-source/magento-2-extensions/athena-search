<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package  Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Helper;

use Magento\Customer\Model\ResourceModel\GroupRepository;
use Magento\Directory\Helper\Data;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Model\Config as TaxConfigHelper;
use Magento\Framework\App\ResourceConnection;
use Syncitgroup\AthenaSearch\Logger\Logger;

class ProductTaxRate extends AbstractHelper
{

    private array $taxRatesCache = [];

    private StoreManagerInterface $storeManager;

    private ResourceConnection $resourceConnection;

    private Logger $logger;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        ResourceConnection $resourceConnection,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->resourceConnection = $resourceConnection;
        $this->logger = $logger;
    }

    /**
     * Get applicable tax rate, if tax is excluded returns 0.0000
     *
     * @param int $taxClassId
     * @return float
     */
    public function getTaxRate(int $taxClassId): float
    {
        $rate = null;
        try {
            $store = $this->storeManager->getStore();
            $priceIncludesTax = (bool) $this->scopeConfig->getValue(
                TaxConfigHelper::CONFIG_XML_PATH_PRICE_INCLUDES_TAX,
                ScopeInterface::SCOPE_STORE,
                $store
            );

            $priceDisplayIncludesTax = (int) $this->scopeConfig->getValue(
                TaxConfigHelper::CONFIG_XML_PATH_PRICE_DISPLAY_TYPE,
                ScopeInterface::SCOPE_STORE,
                $store
            );

            if (!$priceIncludesTax && $priceDisplayIncludesTax !== TaxConfigHelper::DISPLAY_TYPE_EXCLUDING_TAX) {
                $rate = $this->getRate($taxClassId);
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return (float) $rate;
    }

    /**
     * Get Tax rate for product and default customer tax group
     *
     * @param int $productTaxClassId
     * @return float
     */
    private function getRate(int $productTaxClassId): float
    {
        $countryCode = $this->scopeConfig->getValue(
            Data::XML_PATH_DEFAULT_COUNTRY,
            ScopeInterface::SCOPE_STORE
        );

        $rateKey = sprintf(
            '%s-%s-%s',
            $countryCode,
            GroupRepository::DEFAULT_TAX_CLASS_ID,
            $productTaxClassId
        );

        if (empty($this->taxRatesCache[$rateKey])) {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()->from(
                ['tcr' => $connection->getTableName('tax_calculation_rate')],
                ['tcr.rate']
            )->joinLeft(
                ['tc' => $connection->getTableName('tax_calculation')],
                'tcr.tax_calculation_rate_id = tc.tax_calculation_rate_id'
            )->where('tcr.tax_country_id = ?', $countryCode)
            ->where('tc.customer_tax_class_id = ?', GroupRepository::DEFAULT_TAX_CLASS_ID)
            ->where('tc.product_tax_class_id = ?', $productTaxClassId);

            $this->taxRatesCache[$rateKey] = (float) $connection->fetchOne($select);
        }

        return $this->taxRatesCache[$rateKey];
    }
}
