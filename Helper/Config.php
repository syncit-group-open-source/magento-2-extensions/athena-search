<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package  Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Search\Model\QueryFactory;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syncitgroup\AthenaSearch\Controller\V2\Athena;

class Config extends AbstractHelper
{
    public const API_V1 = 'api/v1/';

    public const API_V2 = 'api/v2/';

    private const ENDPOINT_MAP = [
        'suggestions' => 'search-suggestions',
        'autocomplete' => 'autocomplete',
        'first-click' => 'autocomplete/first-click',
        'search' => 'search',
        'search-tabs' => 'search-tabs',
        'swatches' => 'swatches',
        'product-click' => 'product-click',
        'conversion' => 'conversion/cart',
        'order' => 'conversion/order',
        'category-data' => 'category/data'
    ];

    public const ATHENA_CUSTOMER_COOKIE = 'athena_customer_cookie';

    private const DEFAULT_MINISEARCH_TEMPLATE = 'Magento_Search::form.mini.phtml';
    private const ATHENA_MINISEARCH_TEMPLATE = 'Syncitgroup_AthenaSearch::form.mini.phtml';

    private const ATHENA_LAYOUT_GRID = 'athena-grid';
    private const ATHENA_LAYOUT_LIST = 'athena-list';

    private const ATHENA_CSS_ENABLE = 'athena_search/appearance/css';

    private array $productUrlSuffix = [];

    private StoreManagerInterface $storeManager;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
    }

    /**
     * Get status for Athena Search
     */
    public function getAthenaSearchStatus(string $scope = ScopeInterface::SCOPE_STORES, ?int $storeId = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            'athena_search/general/active',
            $scope,
            $storeId
        );
    }

    /**
     * Check athena front end integration mode
     *
     * @return bool
     */
    private function getIntegrationMode(): bool
    {
        return $this->scopeConfig->isSetFlag(
            'athena_search/general/integration_mode',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get base store url
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function baseUrl(): string
    {
        return $this->storeManager->getStore()->getBaseUrl();
    }

    /**
     * Retrieve result page url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     *
     * @param string|null $query
     * @return string
     */
    public function getAthenaFormUrl(?string $query = null): string
    {
        return $this->_getUrl(
            'athena',
            ['_query' => [QueryFactory::QUERY_VAR_NAME => $query], '_secure' => $this->_request->isSecure()]
        );
    }

    /**
     * Get Website Token for Athena Autocomplete
     *
     * @return mixed
     */
    public function getWebsiteToken(?int $storeId = null)
    {
        return $this->scopeConfig->getValue(
            'athena_search/general/wtoken',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get Website Access Token for Athena Analytics
     *
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->scopeConfig->getValue('athena_search/general/access_token', ScopeInterface::SCOPE_STORE);
    }

    /**
     * Is debug mode
     *
     * @return bool
     */
    public function isDebugMode(): ?bool
    {
        return $this->scopeConfig->isSetFlag('athena_search/general/debug');
    }

    /**
     * Get status for InfiniteScroll
     */
    public function getInfiniteScrollStatus(): bool
    {
        return $this->scopeConfig->isSetFlag(
            'athena_search/general/infinite_scroll',
            ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * Get layout for Athena Search landing page
     */
    public function getLayoutHandleSuffix(): ?string
    {
        return $this->scopeConfig->getValue(
            'athena_search/general/selected_layout',
            ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * Get #ID for Athena Search Content Div
     */
    public function getContentDivId(): ?string
    {
        return $this->scopeConfig->getValue(
            'athena_search/general/content_div_id',
            ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * Get #ID for Athena Search Refinement Div
     */
    public function getSidebarDivId(): ?string
    {
        return $this->scopeConfig->getValue(
            'athena_search/general/refinement_div_id',
            ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * Get DOM selector for Athena Search conversion and clicked product analytics
     */
    public function getDomSelector(): ?string
    {
        return $this->scopeConfig->getValue(
            'athena_search/general/dom_selector',
            ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * Get Add to Cart DOM selector for Athena Search conversion analytics
     */
    public function getAddToCartDomSelector(): ?string
    {
        return $this->scopeConfig->getValue(
            'athena_search/general/add_to_cart_dom_selector',
            ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * Get Url for Media folder
     *
     * @return mixed
     */
    public function getMediaPath(): string
    {
        try {
            return $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        } catch (NoSuchEntityException $exception) {
            return '';
        }
    }

    /**
     * Override template if Athena Search is enable
     *
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->getAthenaSearchStatus()
            ? self::ATHENA_MINISEARCH_TEMPLATE
            : self::DEFAULT_MINISEARCH_TEMPLATE;
    }

    /**
     * Get Type of Autocomplete
     *
     * @return string
     */
    public function getAutocompleteLayout(): string
    {
        return $this->scopeConfig->isSetFlag('athena_search/general/layer', ScopeInterface::SCOPE_STORES)
            ? self::ATHENA_LAYOUT_LIST
            : self::ATHENA_LAYOUT_GRID;
    }

    /**
     * Get Loader Image
     *
     * in that case we can ommit getStore call and just use inherited url builder
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getLoaderImage(): string
    {
        return  $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
            . 'athena/loader/loader.gif';
    }

    /**
     * Get Website Url for Athena Dashboard
     *
     * @return string
     */
    public function getAthenaDashboard(): ?string
    {
        return $this->scopeConfig->getValue(
            'athena_search/general/website_url',
            ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * Landing Page Url
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function landingPageUrl(): string
    {
        return $this->baseUrl(). 'athena?q=';
    }

    /**
     * Form url
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function formUrl()
    {
        return $this->baseUrl(). 'athena/';
    }

    /**
     * Get Athena API endpoint url
     *
     * @param string|null $endpoint
     * @param string $apiVersion
     * @return string
     */
    public function getEndpointUrl(string $endpoint, string $apiVersion = self::API_V1): ?string
    {
        if (isset(self::ENDPOINT_MAP[$endpoint])) {
            return $this->getAthenaDashboard()
                . $apiVersion
                . self::ENDPOINT_MAP[$endpoint];
        }
        return null;
    }

    /**
     * Get api request url, in sandbox mode all FE api calls are passed through our controller
     *
     * @param string $endpoint
     * @return string
     */
    public function getRequestUrl(string $endpoint): string
    {
        return $this->getIntegrationMode()
            ? $this->getEndpointUrl($endpoint)
            : $this->_getUrl('athena/v2/athena', [Athena::CALL_ENDPOINT => $endpoint]);
    }

    /**
     * Get stores on which athena search is enabled
     *
     * @return StoreInterface[]
     */
    public function getAthenaEnabledStores(): ?array
    {
        $storesList = [];
        $stores = $this->storeManager->getStores();
        foreach ($stores as $store) {
            $athenaEnabled = $this->getAthenaSearchStatus(
                ScopeInterface::SCOPE_STORE,
                (int) $store->getId()
            );

            if ($athenaEnabled && $this->getWebsiteToken((int) $store->getId()) && $this->getAccessToken()) {
                $storesList[]  = $store;
            }
        }

        return $storesList ?: null;
    }

    /**
     * Get product batch size
     *
     * @return int
     */
    public function getProductBatchSize(): int
    {
        return (int) $this->scopeConfig->getValue('athena_search/general/product_batch_size');
    }

    /**
     * Is data syncing enabled
     *
     * @return bool
     */
    public function isDataSyncEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag('athena_search/general/data_sync_enabled');
    }

    /**
     * Create request header
     *
     * @param string $apiVersion
     * @return array
     * @throws NoSuchEntityException
     */
    public function createRequestHeader(string $apiVersion = self::API_V1): array
    {
        $requestHeaders = [
            'Origin' => substr($this->baseUrl(), 0, -1),
            'Content-Type' => 'application/json',
            'charset'     => 'UTF-8'
        ];

        if ($apiVersion === self::API_V2) {
            $requestHeaders['Authorization'] = 'Bearer ' . $this->getAccessToken();
        }

        return $requestHeaders;
    }

    /**
     * Get product url suffix
     *
     * @param int $storeId
     * @return string
     */
    public function getProductUrlSuffix(int $storeId): string
    {
        if (!isset($this->productUrlSuffix[$storeId])) {
            $this->productUrlSuffix[$storeId] = $this->scopeConfig->getValue(
                'catalog/seo/product_url_suffix',
                ScopeInterface::SCOPE_STORE,
                $storeId
            );
        }
        return $this->productUrlSuffix[$storeId];
    }

    /**
     * Get is Athena Search CSS enabled
     *
     * @return bool
     */
    public function isAthenaStyleEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::ATHENA_CSS_ENABLE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get all stores of WebsiteID
     *
     * @param $websiteId
     * @return array
     * @throws LocalizedException
     */
    public function getStoresList($websiteId): array
    {
        return $this->storeManager->getWebsite($websiteId)->getStores();
    }
}
