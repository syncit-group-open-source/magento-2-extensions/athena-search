<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Cron;

use Magento\Framework\App\ResourceConnection;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Logger\Logger;
use Syncitgroup\AthenaSearch\Model\ResourceModel\JobQueue as JobQueueResource;
use Syncitgroup\AthenaSearch\Model\ResourceModel\JobQueue\CollectionFactory as JobQueueCollectionFactory;

abstract class AbstractAthenaCron
{
    protected ConfigHelper $configHelper;

    protected ResourceConnection $resourceConnection;

    protected JobQueueResource $jobQueueResource;

    protected JobQueueCollectionFactory $jobQueueCollectionFactory;

    protected Logger $logger;

    public function __construct(
        ConfigHelper $configHelper,
        ResourceConnection $resourceConnection,
        JobQueueResource $jobQueueResource,
        JobQueueCollectionFactory $jobQueueCollectionFactory,
        Logger $logger
    ) {
        $this->configHelper = $configHelper;
        $this->resourceConnection = $resourceConnection;
        $this->jobQueueResource = $jobQueueResource;
        $this->jobQueueCollectionFactory = $jobQueueCollectionFactory;
        $this->logger = $logger;
    }

    /**
     * Public method for cron jobs
     *
     * @return void
     */
    public function execute()
    {
        if ($this->configHelper->getAthenaEnabledStores()) {
            $this->process();
        }
    }

    /**
     * Perform task
     *
     * @return void
     */
    abstract protected function process(): void;
}
