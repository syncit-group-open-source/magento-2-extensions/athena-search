<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Cron;

use Syncitgroup\AthenaSearch\Api\Data\JobQueueInterface;

class RetrySyncJob extends AbstractAthenaCron
{
    public const RETRY_LIMIT = 3;

    /**
     * Get all jobs with status error and if retry count is below retry limit set them as pending again
     *
     * @return void
     */
    public function process(): void
    {
        $jobsForRetry = $this->jobQueueCollectionFactory->create()
            ->addFieldToFilter(JobQueueInterface::STATUS, JobQueueInterface::STATUS_ERROR)
            ->addFieldToFilter(JobQueueInterface::RETRY_COUNT, ['lt' => self::RETRY_LIMIT]);

        foreach ($jobsForRetry as $job) {
            $job->setRetryCount($job->getRetryCount() + 1);
            $job->setStatus(JobQueueInterface::STATUS_PENDING);
            try {
                $this->jobQueueResource->save($job);
            } catch (\Exception $exception) {
                $this->logger->logMessage($exception);
            }
        }
    }
}
