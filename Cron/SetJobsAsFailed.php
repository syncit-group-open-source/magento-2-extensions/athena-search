<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Cron;

use Syncitgroup\AthenaSearch\Api\Data\JobQueueInterface;

class SetJobsAsFailed extends AbstractAthenaCron
{
    /**
     * Move jobs to failed status after they have been retried for set amount of times
     *
     * @return void
     */
    public function process(): void
    {
        $connection = $this->resourceConnection->getConnection();
        try {
            $this->resourceConnection->getConnection()->update(
                $connection->getTableName('syncit_athena_jobs_queue'),
                [JobQueueInterface::STATUS => JobQueueInterface::STATUS_FAILED],
                [
                    $connection->quoteInto('status = ?', JobQueueInterface::STATUS_ERROR),
                    $connection->quoteInto('retry_count = ?', RetrySyncJob::RETRY_LIMIT)
                ]
            );
        } catch (\Exception $exception) {
            $this->logger->logMessage($exception);
        }
    }
}
