<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Api\Data;

interface JobQueueInterface
{
    /**
     * STATUS_PENDING published in db, waiting for cron to pick it up
     * STATUS_IN_QUEUE picked for work, before processing
     * STATUS_PROCESSING data currently processed
     * STATUS_FINISHED job finished successfully
     * STATUS_ERROR job had an error while processing
     * STATUS_FAILED error repeated few times, job marked as failed
     * STATUS_CANCELED job canceled, if partial reindex is canceled by full reindex
     */
    public const STATUS_PENDING = 1;
    public const STATUS_IN_QUEUE = 2;
    public const STATUS_PROCESSING = 3;
    public const STATUS_FINISHED = 4;
    public const STATUS_ERROR = 5;
    public const STATUS_FAILED = 6;
    public const STATUS_CANCELED = 7;

    public const JOB_ID = 'job_id';
    public const STATUS = 'status';
    public const MANAGER_CODE = 'manager_code';
    public const CALL_METHOD = 'call_method';
    public const CREATED_AT = 'created_at';
    public const STARTED_AT = 'started_at';
    public const PROCESSED_AT = 'processed_at';
    public const RETRY_COUNT = 'retry_count';
    public const ERROR_MESSAGE = 'error_message';
    public const JOB_HASH_ID = 'job_hash_id';
    public const DATA_TO_PROCESS = 'data_to_process';

    /**
     * Get job id
     *
     * @return string|null
     */
    public function getJobId(): ?string;

    /**
     * Set job status
     *
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status): self;

    /**
     * Get job status
     *
     * @return int
     */
    public function getStatus(): int;

    /**
     * Set manager class code, codes represent di dependency that is later on called in processor
     *
     * @param string $code
     * @return $this
     */
    public function setManagerCode(string $code): self;

    /**
     * Get manager code, use this to call appropriate processor class
     *
     * @return string|null
     */
    public function getManagerCode(): ?string;

    /**
     * Set call method, method that will be called for manager class
     *
     * @param string $callMethod
     * @return $this
     */
    public function setCallMethod(string $callMethod): self;

    /**
     * Get call method
     *
     * @return string|null
     */
    public function getCallMethod(): ?string;

    /**
     * Job creation timestamp
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * Set job started at timestamp
     *
     * @param string $startedAt
     * @return $this
     */
    public function setStartedAt(string $startedAt): self;

    /**
     * Get started at timestamp
     *
     * @return string|null
     */
    public function getStartedAt(): ?string;

    /**
     * Job processed at timestamp
     *
     * @param string $processedAt
     * @return $this
     */
    public function setProcessedAt(string $processedAt): self;

    /**
     * Get processed at timestamp
     *
     * @return string|null
     */
    public function getProcessedAt(): ?string;

    /**
     * Set job hash id value, used to compare incoming jobs to avoid duplicates of same call
     *
     * @param string $jobHashId
     * @return $this
     */
    public function setJobHashId(string $jobHashId): self;

    /**
     * Get job hash value
     *
     * @return string|null
     */
    public function getJobHashId(): ?string;

    /**
     * Get retry count
     *
     * @return int|null
     */
    public function getRetryCount(): ?int;

    /**
     * Set retry count
     *
     * @param int $retryCount
     * @return $this
     */
    public function setRetryCount(int $retryCount): self;

    /**
     * Get Error Message
     *
     * @return string|null
     */
    public function getErrorMessage(): ?string;

    /**
     * Set retry count
     *
     * @param string $errorMessage
     * @return $this
     */
    public function setErrorMessage(string $errorMessage): self;

    /**
     * Set Data to process, this should only ever contain ids of entities to work on
     *
     * @param array|null $data
     * @return $this
     */
    public function setDataToProcess(?array $data): self;

    /**
     * Get Data to process, returns array with entity ids to work on
     *
     * @return array|null
     */
    public function getDataToProcess(): ?array;
}
