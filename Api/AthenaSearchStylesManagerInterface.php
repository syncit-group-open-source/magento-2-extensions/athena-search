<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Api;

interface AthenaSearchStylesManagerInterface
{
    /**
     * Save provided style type
     *
     * @param string $content
     * @param string $path
     * @return string
     */
    public function setStyle(string $content, string $path);

    /**
     * Get styles, whole or by type
     *
     * @param string $styleType
     * @return string
     */
    public function getStyle(string $styleType): string;
}
