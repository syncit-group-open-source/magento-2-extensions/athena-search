define(
    [
        'jquery',
        'jquery/ui',
        'mage/translate',
        'uiComponent',
        'jquery/jquery.cookie'], function ($) {

            return function (config) {
                $(document).ready(
                    function () {
                        setTimeout(
                            function () {
                                userUniqueId(); userToken(); }, 2000
                        );

                        var partKeyword = 0;
                        // Character number of the current sentence being processed
                        var partIndex = 0;
                        // Holds the handle returned from setInterval
                        var intervalVal;
                        // Element that holds the text
                        var elementKey = document.querySelector("#search");

                        var contentVar = [];

                        //rotate keywords in seconds
                        rotateKeywordSeconds = parseInt(config.rotateSec);
                        //keep keywords in seconds
                        keepKeywordSeconds = parseInt(config.keepSec);
                        //delay at start in seconds
                        delayAtStart = parseInt(config.delaySec);

                        //check if not false and not null
                        if(config.suggestionsData !== false && config.suggestionsData !== null) {

                            //delete cookie to prevent showing keyword on other page / refreshed
                            eraseCookie("key pressed");

                            /*
                            Ajax call for Search Suggestions Placeholders and Keywords
                             */
                            //check if is enabled disabled
                            if (config.enabledDisabled === 'on') {

                                $("#search").attr("placeholder", config.placeholderText);

                                //rotate keywords in seconds
                                rotateKeywordSeconds = parseInt(config.rotateSec);
                                //suggestions array
                                //keep keywords in seconds
                                keepKeywordSeconds = parseInt(config.keepSec);
                                //delay at start in seconds
                                delayAtStart = parseInt(config.delaySec);

                                if($(contentVar).length > 0) {

                                    $("#search").attr("placeholder", config.placeholderText + " ").val();

                                    setTimeout(
                                        function () {
                                            intervalVal = setInterval(Type, 70);
                                        }, delayAtStart * 1000
                                    );
                                }

                            } else {
                                if (config.placeholderText !== '' && config.placeholderText !== 'off') {
                                    config.placeholderText = $.mage.__(config.placeholderText);
                                } else {
                                    config.placeholderText = $.mage.__('Search for...');
                                }
                                $("#search").attr("placeholder", config.placeholderText);
                            }
                        }

                        // *   Type word effect
                        function Type()
                        {
                            //check focus of window or tab
                            if (document.hasFocus()) {
                                var keywordsLength = $(contentVar).length;

                                var text = contentVar[partKeyword].substring(0, partIndex + 1);
                                elementKey.placeholder = config.placeholderText + " " + text;
                                partIndex++;

                                if (text === contentVar[partKeyword]) {

                                    //here check cookie and put word in input
                                    var cookieRetrievedValue = getCookieCustom("key pressed");

                                    if (cookieRetrievedValue === "1") {
                                        //stop effect
                                        clearInterval(1);
                                        //write word
                                        elementKey.value = contentVar[partKeyword];
                                        //delete cookie
                                        eraseCookie("key pressed");
                                    }

                                    //clear interval
                                    clearInterval(intervalVal);

                                    //delete word after some specified time
                                    setTimeout(
                                        function () {
                                            intervalVal = setInterval(Delete, 50);
                                        }, config.keepSec * 1000
                                    );
                                }
                            } else {
                                //stop interval, animations when out of focus
                                clearInterval(1);
                            }
                        }

                        // *   Delete word effect
                        function Delete()
                        {
                            var text = contentVar[partKeyword].substring(0, partIndex - 1);
                            elementKey.placeholder = config.placeholderText + " " + text;
                            partIndex--;

                            //if text is deleted start new word
                            if (text === '') {
                                //clear cookie
                                eraseCookie("key pressed");

                                //clear interval
                                clearInterval(intervalVal);

                                if (partKeyword === (contentVar.length - 1)) {
                                    partKeyword = 0;
                                } else {
                                    partKeyword++;
                                }
                                partIndex = 0;

                                setTimeout(
                                    function () {
                                        //when word is deleted start typing again after some time
                                        contentVar = shuffleArray(contentVar);
                                        intervalVal = setInterval(Type, 70);
                                    }, rotateKeywordSeconds * 1000
                                );
                            }
                        }

                        //function to execute function that will enter search suggestion
                        function executeEnteringSearch()
                        {
                            //get values
                            var currentPlaceholder = $.trim(elementKey.placeholder);
                            var defaultPlaceholder = $.trim(config.placeholderText);
                            var sumPlaceholder = config.placeholderText + " " + contentVar[partKeyword];
                            //delete cookie
                            eraseCookie('key pressed');
                            //check if same
                            if (currentPlaceholder === sumPlaceholder) {
                                elementKey.value = contentVar[partKeyword];
                                //delete cookie
                                eraseCookie('key pressed');
                            }
                            //preset and delete cookie
                            if (currentPlaceholder !== defaultPlaceholder) {
                                setCookie('key pressed', '1', '1');
                            } else {
                                //delete cookie
                                eraseCookie('key pressed');
                            }
                        }

                        /* Set cookie */
                        function setCookie(name,value,days)
                        {
                            var expires = '';
                            if (days) {
                                var date = new Date();
                                date.setTime(date.getTime() + (days*24*60*60*1000));
                                expires = '; expires=' + date.toUTCString();
                            }
                            document.cookie = name + '=' + (value || '')  + expires + '; path=/';
                        }

                        /* Get cookie custom */
                        function getCookieCustom(name)
                        {
                            var nameEQ = name + '=';
                            var ca = document.cookie.split(';');
                            for(var i=0;i < ca.length;i++) {
                                var c = ca[i];
                                while (c.charAt(0)==' ') { c = c.substring(1,c.length);
                                }
                                if (c.indexOf(nameEQ) == 0) { return c.substring(nameEQ.length,c.length);
                                }
                            }
                            return null;
                        }

                        /* Arrow right enter autocomplete words */
                        $(elementKey).keydown(
                            function (e) {
                                //check if input is empty
                                if (elementKey.value === '') {
                                    if (e.which == 39 || e.which == 32 || e.which == 13) {
                                        if(e.which == 13) {
                                            e.preventDefault();
                                        }
                                        executeEnteringSearch();
                                    }
                                }
                            }
                        );

                        /* Delete Cookie */
                        function eraseCookie(name)
                        {
                            document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                        }

                        /* Shuffle array */

                        /* Randomize array in-place using Durstenfeld shuffle algorithm */
                        function shuffleArray(array)
                        {
                            for (var i = array.length - 1; i > 0; i--) {
                                var j = Math.floor(Math.random() * (i + 1));
                                var temp = array[i];
                                array[i] = array[j];
                                array[j] = temp;
                            }
                            return array;
                        }

                        /*
                        Disable button after submit search form
                        */
                        $('#search_mini_form').submit(
                            function (e) {

                                var val1 = $('#search').val();
                                let currentFocus = $("#currentFocus").val();

                                if (val1.trim() == "" && parseInt(currentFocus) == -1) {
                                    e.preventDefault();
                                } else {
                                    $(".action-search").prop('disabled', true);
                                }

                                if (currentRequest != null) {
                                    currentRequest.abort();
                                }

                                hideAutocompleteBox();

                                if (currentFocus != "-1") {

                                    /*If the ENTER key is pressed*/
                                    let array = $(".athena-ac-element");
                                    let element_id = array[currentFocus].id;
                                    let id_selector = $("#" + element_id);

                                    if (id_selector.data("type")) {

                                        $(".athena-search-autocomplete").css({'display': 'none'});

                                        let id = id_selector.data("id");
                                        let terms = id_selector.data("terms");
                                        let type = id_selector.data("type");
                                        let url = id_selector.data("url");

                                        $("#athena_id").val(id);
                                        $("#athena_terms").val(terms);
                                        $("#athena_url").val(url);

                                        $("#athena_type").val(type);
                                    }

                                    let select_val = id_selector.children("input").val();
                                    $("#search").val(select_val);

                                    if (id_selector.data("type") == 'popular' || id_selector.data("type") == 'first-popular') {
                                        $("#search").val(id_selector.data("name"));
                                    }

                                }
                            }
                        );

                        var searchForm = $("#search_mini_form");

                        // add layout type from Athena
                        $('.athena-search-autocomplete').addClass(config.layoutType);

                        /*
                        * Handle firstClick or search response success data
                        * */
                        function handleResponseData(data)
                        {
                            $.each(
                                data.data, function (i, item) {

                                    $.each(
                                        item, function (key, value) {
                                            var onlyProduct = 0;

                                            var trackSelectorClass = '';
                                            if (key === 'product') {
                                                trackSelectorClass = 'athena-product-item';
                                                if(!value.items) {
                                                    $('.athena-search-autocomplete:first-of-type')
                                                        .addClass('no-product-content')
                                                }else{
                                                    $('.athena-search-autocomplete:first-of-type')
                                                        .removeClass('no-product-content')
                                                }
                                            }

                                            if (value.items) {
                                                if (value.items.length > 0) {
                                                    $(
                                                        '<div class="athena athena-ac-sec athena-ac-section-'
                                                        + key + ' clearfix"></div>'
                                                    ).prependTo(".athena-flex-ac");
                                                    i_none = 1;

                                                    if (key === 'product') {
                                                        if (onlyProduct != 2) {
                                                            onlyProduct = 1;
                                                        }

                                                    } else {
                                                        onlyProduct = 2;

                                                    }

                                                    //Creating Title Div in Section
                                                    $(
                                                        '<div class="athena-ac-head" id="'
                                                        + key + '"><h4>'
                                                        + value.title + '</h4></div>'
                                                    ).appendTo(".athena-ac-section-" + key);
                                                }
                                            }

                                            //Create Elements Div in Section
                                            $.each(
                                                value.items, function (n, element) {
                                                    var item_id_selector = key + element.id;
                                                    var customerGroupPrices = null;
                                                    var name = element.highlighted_name ? element.highlighted_name : element.name;
                                                    $(
                                                        '<a href="'
                                                        + element.link+'" class="athena-ac-block athena-ac-block-'
                                                        + key +' '
                                                        + trackSelectorClass + '" data-element-id="'
                                                        + element.id+'" > <div class="athena-ac-element clearfix" id="'
                                                        + item_id_selector + '" data-name="'
                                                        + element.name + '" data-type="'
                                                        + key + '" data-id="'
                                                        + element.id + '" data-terms="'
                                                        + searchForClickProduct + '" data-url="'
                                                        + element.link + '"><div class="athena-el-2 athena-el-2-'
                                                        + item_id_selector + '" id="'
                                                        + item_id_selector + '"><span class="athena-name athena-name-sec-'
                                                        + key + '" title="'
                                                        + element.name + '">'
                                                        + name
                                                        + '</span></div></div></a>'
                                                    ).appendTo(
                                                        ".athena-ac-section-"
                                                        + key
                                                    );


                                                    if (element.image) {
                                                        $('.athena-ac-element').removeClass('athena-element-' + key + '-no-image').addClass('athena-element-' + key);
                                                        $(
                                                            '<div class="athena-el-1" id="'
                                                            + item_id_selector + '"> <img class="athena-img athena-img-sec-'
                                                            + key + '" src="'
                                                            + element.image + '" alt="'
                                                            + element.name + '"></div>'
                                                        ).prependTo('#'+item_id_selector);

                                                        if (key === 'product') {
                                                            if (!element.customer_group_prices) {
                                                                if (element.price) {
                                                                    if (element.price.special_price_with_currency) {
                                                                        $('<span class="athena-price athena-price-when-sale">' + element.price.regular_price_with_currency + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                        $('<span class="athena-sale-price">' + element.price.special_price_with_currency + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                    } else {
                                                                        $('<span class="athena-price">' + element.price.regular_price_with_currency + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                    }
                                                                }
                                                            } else {
                                                                customerGroupPrices = Object.values(element.customer_group_prices)[0];
                                                                if (customerGroupPrices.special_price) {
                                                                    $('<span class="athena-price athena-price-when-sale">' + customerGroupPrices.regular_price_with_currency + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                    $('<span class="athena-sale-price">' + customerGroupPrices.special_price_with_currency + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                } else {
                                                                    $('<span class="athena-price">' + customerGroupPrices.regular_price_with_currency + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if (key != 'popular') {
                                                            $('.athena-ac-element').removeClass('athena-element-' + key).addClass('athena-element-' + key + '-no-image');
                                                        } else {
                                                            $('.athena-ac-element').removeClass('athena-element-' + key).addClass('athena-element-' + key + '-no-image');
                                                            $('.athena-ac-block-' + key).find("[data-element-id='" + element.id + "']").attr('href', config.landingPageUrl+element.name_urlencoded);
                                                        }

                                                        if (key === 'product') {
                                                            $('<span class="athena-price"></span><span class="athena-sale-price"></span>').appendTo(".athena-el-2-" + item_id_selector);
                                                            $('<span class="athena-price-only"></span>').appendTo(".athena-el-2-" + item_id_selector);
                                                            if (!element.customer_group_prices) {
                                                                if (element.autocomplete_sale_price) {
                                                                    $('<span class="athena-price athena-price-when-sale">' + element.autocomplete_price + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                    $('<span class="athena-sale-price">' + element.autocomplete_sale_price + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                } else {
                                                                    $('<span class="athena-price-only">' + element.autocomplete_price + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                }
                                                            } else {
                                                                customerGroupPrices = Object.values(element.customer_group_prices)[0];
                                                                if (customerGroupPrices.special_price) {
                                                                    $('<span class="athena-price athena-price-when-sale">' + customerGroupPrices.regular_price_with_currency + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                    $('<span class="athena-sale-price">' + customerGroupPrices.special_price_with_currency + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                } else {
                                                                    $('<span class="athena-price-only">' + customerGroupPrices.regular_price_with_currency + '</span>').appendTo(".athena-el-2-" + item_id_selector);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            );
                                        }
                                    );

                                    //Banner on Top
                                    if (i === 'banners' && item.top) {
                                        createTopBanner(item);
                                    }

                                    //Banner on Bottom
                                    if (i === 'banners' && item.bottom) {
                                        createBottomBanner(item);
                                    }
                                }
                            );
                        }

                        function afterSearchResult(val)
                        {
                            var i_none = 0;
                            var onlyProduct = 0;
                            var maximumData = null;

                            $('#search_autocomplete .athena-flex .athena').each(
                                function () {
                                    var valueData = $(this).attr('data-order');
                                    maximumData = (valueData > maximumData) ? valueData : maximumData;
                                    if(maximumData == $(this).attr('data-order')) {
                                        $(this).removeClass('last-search-result')
                                        $(this).addClass('last-search-result')
                                    }
                                }
                            );

                            if (onlyProduct == 1) {
                                $('.athena-search-autocomplete:first-of-type').addClass('only-product');
                            }else{
                                $('.athena-search-autocomplete:first-of-type').removeClass('only-product')
                            }
                            if (i_none === 1) {
                                showAutocompleteBox();
                                hideNoResult();
                            } else {
                                showAutocompleteBox();
                                showNoResult(val);
                            }

                            $('.athena-search-tabs').css({'display': 'flex'});
                            $("#numRes").val(i_none);
                        }

                        /*
                        * Create search tab
                        * */
                        function createSearchTab(item)
                        {
                            if (item.checked) {
                                var checked = 'checked';
                            } else {
                                checked = '';
                            }

                            $(
                                '<div class="athena-tab athena-tabs-'
                                +item.value+' athena-tabs-category-'
                                +item.value+'"><input type="radio" id="'
                                +item.title+'" class="athena-tabs-radio athena-tabs-radio-'
                                +item.value+'" value="'
                                +item.value+'" '+checked+' ><label for="'
                                +item.title+'">'
                                +item.title+'</label></div>'
                            ).appendTo(".athena-search-tabs");
                        }

                        /*
                        * Create top banner
                        * */
                        function createTopBanner(item)
                        {
                            //Creating Section Div in Autocomplete
                            $('<div class="athena athena-ac-sec athena-ac-section-banner_top athena-banner-section-banner_top clearfix banner-top" id="banner_top"></div>').prependTo(".athena-autocomplete");
                            $.each(
                                item.top, function (n, element) {
                                    //Add Html for Banner
                                    $(element.html).prependTo(".athena-banner-section-banner_top");
                                }
                            );
                        }

                        /*
                        * Create bottom banner
                        */
                        function createBottomBanner(item)
                        {
                            if (item.bottom) {
                                //Creating Section Div in Autocomplete
                                $('<div class="athena athena-ac-sec athena-ac-section-banner_bottom athena-banner-section-banner_bottom clearfix banner-bottom" id="banner_bottom"></div>')
                                    .appendTo(".athena-autocomplete");
                                $.each(
                                    item.bottom, function (n, element) {
                                        //Add Html for Banner
                                        $(element.html).prependTo(".athena-banner-section-banner_bottom");
                                    }
                                );
                            }
                        }

                        /*
                        * Remove old results in dropdown
                        * */
                        function startFresh()
                        {
                            $(".athena-flex-ac").empty();
                            $(".athena-ac-sec").remove();
                            $(".athena-ac-section-banner_bottom").remove();
                            $(".athena-ac-section-banner_top").remove();
                            $(".athena-banner-section-banner_top").remove();
                            $(".athena-banner-section-banner_bottom").remove();
                        }

                        /*
                        Show Autocomplete
                        */
                        function showAutocompleteBox()
                        {
                            $(".athena-autocomplete").css({'display': 'block'});
                            searchForm.addClass('arrow');
                        }

                        /*
                        Hide Autocomplete
                         */
                        function hideAutocompleteBox()
                        {
                            $(".athena-autocomplete").css({'display': 'none'});
                        }

                        /*
                        Show "No Result" Section in Autocomplete
                        */
                        function showNoResult(value)
                        {
                            $(".athena-no-result").show();
                            $("#athena-no-result-query").html('"' + value + '"');
                        }

                        /*
                        Hide "No Result" Section in Autocomplete
                         */
                        function hideNoResult()
                        {
                            $(".athena-no-result").hide();
                        }

                        /*
                        Main Search Function
                        */
                        var searchForClickProduct = '';
                        var currentRequest = null;
                        //Creating Section for Search Tabs
                        $.ajax(
                            {
                                type: 'POST',
                                data: {'token': config.websiteToken},
                                url: config.searchTabsUrl,
                                cache: true,
                                global: false,
                                success: function (searchTabs) {
                                    $.each(
                                        searchTabs.data, function (i, item) {
                                            if (item) {
                                                createSearchTab(item);
                                            }
                                        }
                                    );
                                }
                            }
                        );

                        $(".block-search").on(
                            'change', '.athena-tabs-radio', function (e) {
                                $(".athena-tabs-radio").prop('checked', false);
                                $("."+this.classList[1]).prop('checked', true);
                                $(".athena-categories-tabs").remove();

                                search();
                            }
                        );

                        /*
                        Main search function for Autocomplete
                         */
                        function search()
                        {
                            startFresh();
                            currentFocus = -1;
                            let val = $('#search').val();
                            let storedResult;

                            if (localStorage.getItem('searchResult')) {
                                storedResult = JSON.parse(localStorage.getItem('searchResult'));

                                if (val != storedResult.term) {
                                    localStorage.removeItem('searchResult');
                                }
                            }

                            if (typeof(val) == "undefined" || val.length === 0) {
                                firstClick();
                                $('#athena-search-tabs').css({'display': 'none'});
                            } else {
                                if (storedResult && val == storedResult.term) {
                                    handleResponseData(storedResult.value);
                                    afterSearchResult(val);
                                } else {
                                    var res = val.replace('<>', '');
                                    searchForClickProduct += res + ',';

                                    var searchTab = $(".athena-tabs-radio:checked").val();
                                    currentRequest = $.ajax(
                                        {
                                            type: 'GET',
                                            data: {
                                                'token': config.websiteToken,
                                                'q': val,
                                                'customer_group_id': config.customerGroupId,
                                                'autocomplete_tab': searchTab,
                                                'customer': getCookie('athena_customer_cookie'),
                                                'athenaUid': localStorage.getItem("_athena-uid")
                                            },
                                            url: config.athenaAutocompleteUrl,
                                            cache: true,
                                            global: false,
                                            beforeSend: function () {
                                                if (currentRequest != null) {
                                                    currentRequest.abort();
                                                }
                                            },
                                            success: function (data) {
                                                if (data.data) {
                                                    const item = {
                                                        value: data,
                                                        term: val
                                                    }
                                                    localStorage.setItem('searchResult', JSON.stringify(item));
                                                }
                                                handleResponseData(data);
                                                afterSearchResult(val);
                                            }
                                        }
                                    );
                                }
                            }
                        }

                        /*
                        Delay Function
                         */
                        function delay(callback, ms)
                        {
                            let timer = 0;

                            return function () {
                                let context = this, args = arguments;
                                clearTimeout(timer);

                                timer = setTimeout(
                                    function () {
                                        callback.apply(context, args);
                                    }, ms || 0
                                );
                            };
                        }

                        /*
                        Event KeyUp
                        */
                        $('#search').keyup(
                            delay(
                                function (e) {
                                    if (e.keyCode == 13 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                                        if (e.keyCode == 13) {
                                            if (currentRequest != null) {
                                                currentRequest.abort();
                                            }
                                            hideAutocompleteBox();
                                        }
                                        return false;
                                    }
                                    search();
                                }, 100
                            )
                        );

                        /*
                        Disable on Keyword Enter click on autocomplete items
                         */
                        $(window).keydown(
                            function (e) {
                                if (e.keyCode == 13) {
                                    $("#currentFocus").val(-1);
                                }
                            }
                        );

                        /*
                         Click on product in autocomplete
                         */
                        $(".athena-search-autocomplete").on(
                            'mousedown touchstart', config.domSelector, function (e) {
                                productClick(e, 'autocomplete');
                            }
                        );

                        /*
                        Get Cookie by key
                         */
                        function getCookie(key)
                        {
                            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
                            return keyValue ? keyValue[2] : null;
                        }

                        /*
                        Search History
                         */
                        function searchHistory(productId)
                        {
                            var date = Date.now() + 86400;
                            var searchHistory = localStorage.getItem('athenaSearchHistory');
                            if (searchHistory) {
                                searchHistory = JSON.parse(searchHistory);
                                if (searchHistory[productId] && searchHistory[productId]['token'] === config.websiteToken) {
                                    searchHistory[productId]['token'] = config.websiteToken;
                                    searchHistory[productId]['expiration'] = date;
                                    searchHistory[productId]['oid'] = productId;
                                    localStorage.setItem('athenaSearchHistory', JSON.stringify(searchHistory));
                                } else {
                                    searchHistory[productId] = {};
                                    searchHistory[productId]['token'] = config.websiteToken;
                                    searchHistory[productId]['expiration'] = date;
                                    searchHistory[productId]['oid'] = productId;
                                    localStorage.setItem('athenaSearchHistory', JSON.stringify(searchHistory));
                                }
                            } else {
                                const searchHistory = {};
                                searchHistory[productId] = {};
                                searchHistory[productId]['token'] = config.websiteToken;
                                searchHistory[productId]['expiration'] = date;
                                searchHistory[productId]['oid'] = productId;
                                localStorage.setItem('athenaSearchHistory', JSON.stringify(searchHistory));
                            }

                            return date;
                        }

                        /*
                        Get User Token
                         */
                        function userToken()
                        {
                            var token = getCookie("_athena");

                            if (!token) {
                                token = 'anonymous-'+Math.random().toString(36).substr(2, 9)
                                    + '-'
                                    + Math.random().toString(26).substr(2, 9);
                                $.cookie("_athena", token, { path: '/' });
                            }

                            return token;
                        }

                        /*
                        Get User Unique ID
                         */
                        function userUniqueId()
                        {
                            var token = localStorage.getItem("_athena-uid");

                            if (!token) {
                                token = Math.random().toString(36).substr(2, 9)
                                    + '-'
                                    + Math.random().toString(26).substr(2, 9)
                                    + '-'
                                    + Math.random().toString(26).substr(2, 9)
                                    + '-'
                                    + Math.random().toString(26).substr(2, 9);

                                localStorage.setItem("_athena-uid", token);
                            }

                            return token;
                        }

                        /*
                        Send Clicked Product to Athena
                         */
                        function productClick(e, type)
                        {
                            var currentTarget = e.currentTarget;
                            var searchKeywords = "";

                            if (typeof currentTarget.dataset.elementId != 'undefined') {
                                var productId = currentTarget.dataset.elementId;
                                var productElement = $("#product"+productId);
                                searchKeywords = "First Click,";
                                searchHistory(productId);

                                if (productElement.length) {
                                    searchKeywords = productElement[0].dataset.terms;
                                }

                                $.ajax(
                                    {
                                        type: 'POST',
                                        cache: true,
                                        data: {
                                            'productId': productId,
                                            'type': type,
                                            'token': config.websiteToken,
                                            'searchKeywords': searchKeywords,
                                            'customer': getCookie('athena_customer_cookie'),
                                            'athenaUid': localStorage.getItem("_athena-uid")
                                        },
                                        url: config.productClickApiUrl,
                                        dataType: "json",
                                        beforeSend: function () {
                                            if (currentRequest != null) {
                                                currentRequest.abort();
                                            }
                                        },
                                        error: function (request, error) {
                                            //
                                        },
                                        success: function (data) {
                                            //
                                        }
                                    }
                                );
                            }
                        }

                        /*
                        Function for first click
                         */
                        function firstClick()
                        {
                            $('.athena-search-tabs').css({'display': 'none'});
                            currentFocus = -1;
                            let val = $('#search').val();
                            var now = Date.now();
                            var ttl = 1800000;

                            if (localStorage.getItem('firstClickData')) {
                                var stored = JSON.parse(localStorage.getItem('firstClickData'));

                                if (now > stored.expiry) {
                                    localStorage.removeItem('firstClickData')
                                }
                            }

                            if (val.length === 0 && !(localStorage.getItem('firstClickData'))) {
                                currentRequest = $.ajax(
                                    {
                                        type: 'POST',
                                        global: false,
                                        data: {
                                            'token': config.websiteToken,
                                            'customer': getCookie('athena_customer_cookie'),
                                            'athenaUid': localStorage.getItem("_athena-uid"),
                                            'customer_group_id': config.customerGroupId,
                                        },
                                        url: config.athenaAutocompleteFirstClickUrl,
                                        beforeSend: function () {
                                            if (currentRequest != null) {
                                                currentRequest.abort();
                                            }
                                        },
                                        success: function (data) {
                                            if (data.data) {
                                                const item = {
                                                    value: data,
                                                    expiry: now + ttl,
                                                }
                                                localStorage.setItem('firstClickData', JSON.stringify(item));
                                            }
                                            handleResponseData(data);
                                            showAutocompleteBox();
                                        }
                                    }
                                );
                            } else {
                                var stored = JSON.parse(localStorage.getItem('firstClickData'));
                                handleResponseData(stored.value);
                                showAutocompleteBox();
                            }
                        }

                        /*
                        Event OnClick
                        */
                        $(document).on(
                            'click', function (event) {
                                let tar = event.target.id;
                                let tarClass = event.target.classList[0];
                                let tagName = event.target.tagName;
                                let event_type = event.type;
                                let tarParentClass = event.target.parentElement.classList[0];

                                let val = $('#search').val();

                                //Current Focus
                                currentFocus = -1;

                                if (tar === 'search'
                                    || tar === 'search_autocomplete'
                                    || tarClass === 'athena'
                                    || tarClass === 'action-search'
                                    || tarClass === 'pe-7s-search'
                                    || tagName === 'H4'
                                    || event_type === 'touchmove'
                                    || tarParentClass === 'athena-autocomplete'
                                    || tarParentClass === 'athena-tab'
                                    || tarParentClass === 'athena-search-tabs'
                                ) {
                                    $(".search-form").addClass('overlay');
                                    $("#search_mini_form").addClass('overlay');
                                    search();
                                } else {
                                    $('.athena-ac-sec p iframe').remove();
                                    $(".search-close").removeClass("active");
                                    $(".search-form").removeClass("active overlay");
                                    hideAutocompleteBox();
                                    searchForm.removeClass('arrow');
                                    $("html, body").removeClass('noscroll');
                                }
                            }
                        );

                        /*
                        Disable button after submit search form
                        */
                        $('#search_mini_form').submit(
                            function (e) {

                                var val1 = $('#search').val();
                                let currentFocus = $("#currentFocus").val();

                                if (val1.trim() == "" && parseInt(currentFocus) == -1) {
                                    e.preventDefault();
                                } else {
                                    $(".action-search").prop('disabled', true);
                                }

                                if (currentRequest != null) {
                                    currentRequest.abort();
                                }
                                hideAutocompleteBox();

                                if (currentFocus != "-1") {

                                    /*If the ENTER key is pressed*/
                                    let array = $(".athena-ac-element");
                                    let element_id = array[currentFocus].id;
                                    let id_selector = $("#" + element_id);

                                    if (id_selector.data("type")) {

                                        $(".athena-search-autocomplete").css({'display': 'none'});

                                        let id = id_selector.data("id");
                                        let terms = id_selector.data("terms");
                                        let type = id_selector.data("type");
                                        let url = id_selector.data("url");

                                        $("#athena_id").val(id);
                                        $("#athena_terms").val(terms);
                                        $("#athena_url").val(url);

                                        $("#athena_type").val(type);
                                    }

                                    let select_val = id_selector.children("input").val();
                                    $("#search").val(select_val);

                                    if (id_selector.data("type") == 'popular' || id_selector.data("type") == 'first-popular') {
                                        $("#search").val(id_selector.data("name"));
                                    }

                                }
                            }
                        );

                        $(document).on(
                            "touchstart", ".athena-wrapper", function () {
                                $("#search").blur();
                            }
                        );

                        // Clear Search

                        $('#search').val('');

                        /*
                        Search Focus
                        */
                        let inp = $('#search');
                        let currentFocus = -1;

                        inp.on(
                            "keydown", function (e) {
                                let keyCode = e.keyCode;
                                let total_num_search = $(".athena-ac-element").length;

                                if (keyCode == 40) {
                                    /*If the arrow DOWN key is pressed*/

                                    currentFocus++;
                                    if (currentFocus === total_num_search) {
                                        currentFocus = 0;
                                    }

                                    let array = $(".athena-ac-element");
                                    let element_id = array[currentFocus].id;

                                    let id_selector = $("#" + element_id);

                                    id_selector.addClass('syncit-autocomplete-focus');

                                } else if (keyCode == 38) {
                                    /*If the arrow UP key is pressed*/

                                    let array = $(".athena-ac-element");
                                    currentFocus--;

                                    if (currentFocus < 0) {
                                        currentFocus = total_num_search - 1;
                                    }

                                    let element_id = array[currentFocus].id;
                                    let id_selector = $("#" + element_id);
                                    id_selector.addClass('syncit-autocomplete-focus');
                                } else if (keyCode == 37) {

                                    currentFocus = -1;
                                    $("#currentFocus").val(currentFocus);
                                } else if (keyCode == 39) {

                                    currentFocus = -1;
                                    $("#currentFocus").val(currentFocus);
                                } else {
                                    $("#athena_type").val('search');
                                }

                                $("#currentFocus").val(currentFocus);
                            }
                        );

                        // Mouseover Focus
                        $(".block-search").on(
                            'vclick', function (e) {
                                if ($("#search").val() != '') {

                                    let element_class = e.target.classList[1];
                                    let element_class_0 = e.target.classList[0];
                                    let tagName = e.target.tagName;
                                    var href = '';

                                    if (element_class == 'athena-ac-element'
                                        || element_class == 'athena-element-first-product'
                                    ) {

                                        if (typeof e.target.closest("a") != 'undefined') {
                                            href = e.target.closest("a").href;
                                        }
                                        if (href != '') {
                                            window.location.href = href;
                                        }
                                        return false;
                                    }

                                    if (element_class_0 == 'athena-img'
                                        || element_class_0 == 'athena-name'
                                        || element_class_0 == 'athena-price-only'
                                        || element_class_0 == 'athena-price'
                                        || element_class_0 == 'athena-sale-price'
                                    ) {
                                        if (typeof e.target.closest("a") != 'undefined') {
                                            href = e.target.closest("a").href;
                                        }
                                        if (href != '') {
                                            window.location.href = href;
                                        }
                                        return false;
                                    }

                                    if (element_class_0 == 'athena-highlight'
                                        || element_class_0 == 'highlightSearchFont'
                                    ) {
                                        if (typeof e.target.closest("a") != 'undefined') {
                                            href = e.target.closest("a").href;
                                        }
                                        if (href != '') {
                                            window.location.href = href;
                                        }
                                        return false;
                                    }

                                    if (element_class != 'athena-ac-element'
                                        && tagName != 'B'
                                        && tagName != 'SPAN'
                                        && element_class_0 != 'athena-img'
                                        && element_class_0 != 'athena-el-2'
                                        && element_class_0 != 'athena-sale-price'
                                        && element_class_0 != 'athena-name'
                                        && element_class_0 != 'athena-highlight'
                                    ) {
                                        $("#athena_type").val('search');
                                        currentFocus = -1;
                                    }
                                }
                            }
                        );

                        $('.block-search').mouseleave(
                            function () {
                                currentFocus = -1;
                                $("#currentFocus").val(currentFocus);
                            }
                        );
                    }
                );
            }
        }
);

