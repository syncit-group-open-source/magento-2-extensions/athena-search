<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Block\System\Config\Button;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;

class DataSyncingStatus extends Field
{
    private ConfigHelper $configHelper;

    protected $_template = 'Syncitgroup_AthenaSearch::system/config/button/indexing-status.phtml';

    public function __construct(
        Context $context,
        ConfigHelper $configHelper,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        $this->configHelper = $configHelper;
        parent::__construct($context, $data, $secureRenderer);
    }

    /**
     * Unset default values and render
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element): string
    {
        return parent::render($element->unsScope()
            ->unsCanUseWebsiteValue()
            ->unsCanUseDefaultValue()
        );
    }

    /**
     * Wrapper method
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }

    /**
     * Return button html
     *
     * @return string
     * @throws LocalizedException
     */
    public function getButtonHtml(): string
    {
        return $this->getLayout()
            ->createBlock('Magento\Backend\Block\Widget\Button')
            ->setData(
                [
                    'id' => 'data_sync_status',
                    'label' => $this->getDataSyncStatus() ? __('ENABLED') : __('DISABLED'),
                    'class' => $this->getDataSyncStatus()
                        ? 'data-sync-enabled' : 'data-sync-disabled'
                ]
            )->toHtml();
    }

    /**
     * Get data sync status wrapper
     *
     * @return bool
     */
    public function getDataSyncStatus(): bool
    {
        return $this->configHelper->isDataSyncEnabled();
    }
}
