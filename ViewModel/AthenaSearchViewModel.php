<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Aleksa Zivkovic <aleksa.zivkovic@syncitgroup.com>
 * @copyright Copyright (C) 2021 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\ViewModel;

use GuzzleHttp\ClientFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Syncitgroup\AthenaSearch\Helper\Config as ConfigHelper;
use Syncitgroup\AthenaSearch\Service\Api\AthenaSearchStylesManager;
use Syncitgroup\AthenaSearch\Serializer\Json;
use Magento\Customer\Model\Session as CustomerSession;
use Syncitgroup\AthenaSearch\Logger\Logger;
use Magento\Framework\App\CacheInterface;

class AthenaSearchViewModel implements ArgumentInterface
{
    private const CACHE_KEY = 'athena_search_suggestions_cache';

    public const ROTATION_IN_SEC = 3;

    public const KEEP_KEYWORD_SEC = 1;

    public const DELAY_AT_START_SEC = 0;

    private ClientFactory $clientFactory;

    private ConfigHelper $configHelper;

    private AthenaSearchStylesManager $athenaSearchStylesManager;

    private CustomerSession $customerSession;

    private CustomerRepositoryInterface $customerRepository;

    private HttpContext $httpContext;

    private CacheInterface $cache;

    private Logger $logger;

    private Json $serializer;

    public function __construct(
        ClientFactory $clientFactory,
        ConfigHelper $configHelper,
        AthenaSearchStylesManager $athenaSearchStylesManager,
        CustomerSession $session,
        CustomerRepositoryInterface $customerRepository,
        HttpContext $httpContext,
        CacheInterface $cache,
        Logger $logger,
        Json $serializer
    ) {
        $this->clientFactory = $clientFactory;
        $this->configHelper = $configHelper;
        $this->athenaSearchStylesManager = $athenaSearchStylesManager;
        $this->customerSession = $session;
        $this->customerRepository = $customerRepository;
        $this->httpContext = $httpContext;
        $this->cache = $cache;
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    /**
     * Get Athena configuration helper instance
     *
     * @return ConfigHelper
     */
    public function getConfigHelper(): ConfigHelper
    {
        return $this->configHelper;
    }

    /**
     * Get stored athena search style by type
     *
     * @param string $styleType
     * @return string
     */
    public function getStyle(string $styleType): string
    {
        return $this->athenaSearchStylesManager->getStyle($styleType);
    }

    /**
     * Get customer group id
     *
     * @return int|null
     */
    public function getCustomerGroupId(): ?int
    {
        try {
            if ($this->customerSession->getCustomerId()) {
                $customer = $this->customerRepository->getById(
                    $this->customerSession->getCustomerId()
                );
                $customerGroupId = (int) $customer->getGroupId();
            } elseif ($this->httpContext->getValue(Context::CONTEXT_AUTH)) {
                $customerGroupId = (int) $this->httpContext->getValue(Context::CONTEXT_GROUP);
            } elseif ($this->customerSession->isLoggedIn()) {
                $customer = $this->customerRepository->getById(
                    $this->customerSession->getCustomer()->getId()
                );
                $customerGroupId = (int) $customer->getGroupId();
            } else {
                $customerGroupId = 0;
            }
            return $customerGroupId;
        } catch (\Exception $exception) {
            return 0;
        }
    }

    /**
     * Get Placeholder text for use in js
     *
     * @param $placeholder
     * @return string
     */
    public function getPlaceholderText($placeholder): ?string
    {
        !in_array($placeholder, ['', 'off'])
            ? $placeholderText = __($placeholder)
            : $placeholderText = __('Search for...');

        return $placeholderText->getText();
    }

    public function getSearchSuggestionData(): DataObject
    {
        $data = $this->cache->load(self::CACHE_KEY);
        if ($data === false) {
            $data = $this->fetchSearchSuggestionData();

            if (!empty($data)) {
                $this->cache->save(
                    $this->serializer->serialize($data),
                    self::CACHE_KEY
                );

            }
        }

        return is_string($data)
            ? new DataObject($this->serializer->unserialize($data))
            : new DataObject();
    }

    /**
     * Make a request for search suggestions data
     *
     * @return array
     */
    private function fetchSearchSuggestionData(): array
    {
        $searchSuggestionData = [];
        if ($this->configHelper->getAthenaSearchStatus()) {

            $data = [
                'token' => $this->configHelper->getWebsiteToken()
            ];

            try {
                $client = $this->clientFactory->create();
                $response = $client->post(
                    $this->configHelper->getEndpointUrl('suggestions', ConfigHelper::API_V2),
                    [
                        'headers' => $this->configHelper->createRequestHeader(ConfigHelper::API_V2),
                        'body' => $this->serializer->serialize($data)
                    ]
                );
                $stream = $response->getBody();
                $contents = $stream->getContents();
                if (!empty($contents)) {
                    $searchSuggestionData = $this->serializer->unserialize($contents);
                    $searchSuggestionData['is_set'] = true;
                }
            } catch (\Exception $exception) {
                $this->logger->logMessage($exception);
            }
        }

        return $searchSuggestionData;
    }

    public function serialize($data)
    {
        return $this->serializer->serialize($data);
    }
}
