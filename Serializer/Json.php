<?php
declare(strict_types=1);

/**
 * SyncIt Group
 *
 * This source file is subject to the SyncIt Software License, which is available at https://syncitgroup.com/.
 * Do not edit or add to this file if you wish to upgrade to the newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  SyncIt
 * @package   Syncitgroup_AthenaSearch
 * @author    Jovana Branković <jovana.brankovic@syncitgroup.com>
 * @copyright Copyright (C) 2022 SyncIt (https://syncitgroup.com/)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://syncitgroup.com/
 */

namespace Syncitgroup\AthenaSearch\Serializer;

class Json
{
    /**
     * Serialize data into string
     *
     * @param mixed $value
     * @param int|null $flags
     * @param int $depth
     * @return string|false
     * @throws \InvalidArgumentException
     */
    public function serialize($value, int $flags = 0, int $depth = 512)
    {
        $result = json_encode($value, $flags, $depth);
        if (false === $result) {
            throw new \InvalidArgumentException(
                "Unable to serialize value. Error: " . json_last_error_msg()
            );
        }
        return $result;
    }

    /**
     * Unserialize the given string
     *
     * @param string $json
     * @param bool|null $associative
     * @param int $depth
     * @param int $flags
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function unserialize(
        string $json,
        ?bool $associative = true,
        int $depth = 512,
        int $flags = 0
    ) {
        $result = json_decode($json, $associative, $depth, $flags);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \InvalidArgumentException(
                "Unable to unserialize value. Error: " . json_last_error_msg()
            );
        }
        return $result;
    }
}
